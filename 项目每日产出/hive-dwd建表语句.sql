
drop table if exists onl_edu_dwd.ft_customer_relationship;
create table if not exists onl_edu_dwd.ft_customer_relationship(
    dt                          	int ,
    create_date_time                string        comment '创建时间',
    update_date_time                string        comment '最后更新时间',
    customer_id                     int           comment '所属客户id',
    first_id                        int           comment '第一条客户关系id',
    business_scrm_department_id     int           comment '归属部门',
    origin_type                     string        comment '数据来源',
    itcast_school_id                int           comment '校区Id',
    itcast_subject_id               int           comment '学科Id',
    creator                         int           comment '创建人',
    origin_channel                  string        comment '来源渠道'
) comment '意向信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

drop table if exists onl_edu_dwd.dt_customer;
create table if not exists onl_edu_dwd.dt_customer(
    id                       int ,
    customer_relationship_id int      comment '当前意向id',
    create_date_time         string   comment '创建时间',
    update_date_time         string   comment '最后更新时间',
    name                     string   comment '姓名',
    area                     string   comment '所在区域',
    creator                  int      comment '创建人ID',
    origin_type              string   comment '数据来源',
    origin_channel           string   comment '来源渠道'
) comment '员工信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


drop table if exists onl_edu_dwd.dt_customer_appeal;
create table if not exists onl_edu_dwd.ft_customer_appeal(
    id                             int ,
    customer_relationship_first_id int     comment '第一条客户关系id',
    appeal_status                  int     comment '申诉状态，0:待稽核 1:无效 2：有效',
    create_date_time               string  comment '创建时间（申诉时间）',
    update_date_time               string  comment '更新时间'
) comment '线索申诉信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


drop table if exists onl_edu_dwd.ft_customer_clue;
create table if not exists onl_edu_dwd.dt_customer_clue(
    id                       int,
    create_date_time         string    comment '创建时间',
    update_date_time         string    comment '最后更新时间',
    clue_state               string    comment '线索状态',
    customer_id              int       comment '客户id',
    customer_relationship_id int       comment '客户关系id'
) comment '线索信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


drop table if exists onl_edu_dwd.dt_employee;
create table if not exists onl_edu_dwd.dt_employee(
    id                  int ,
    department_id       string      comment 'OA中的部门编号，有负值',
    department_name     string      comment 'OA中的部门名',
    creator             string      comment '创建人',
    create_date_time    string      comment '创建时间',
    update_date_time    string      comment '最后更新时间',
    tdepart_id          int         comment '直属部门'
) comment '部门信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

drop table if exists onl_edu_dwd.dt_itcast_school;
create table if not exists onl_edu_dwd.dt_itcast_school(
    id               int ,
    create_date_time string     comment '创建时间',
    update_date_time string     comment '最后更新时间',
    name             string     comment '校区名称'
) comment '校区信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


drop table if exists onl_edu_dwd.dt_itcast_subject;
create table if not exists onl_edu_dwd.dt_itcast_subject(
    id               int,
    create_date_time string    comment '创建时间',
    update_date_time string    comment '最后更新时间',
    name             string    comment '学科名称'
) comment '学科信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

drop table if exists onl_edu_dwd.dt_scrm_department;
create table if not exists onl_edu_dwd.dt_scrm_department(
    id               int,
    name             string     comment '部门名称',
    create_date_time string     comment '创建时间',
    update_date_time string     comment '更新时间',
    tdepart_code     int        comment '直属部门'
) comment '部门信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');