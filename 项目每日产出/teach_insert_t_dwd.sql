insert into onl_edu_dwd.dt_class_studying_student_count
select id,
       school_id,
       subject_id,
       class_id,
       studying_student_count,
       studying_date
from onl_edu_ods.class_studying_student_count;

insert into onl_edu_dwd.dt_course_table_upload_detail
select id,
       base_id,
       class_id,
       class_date,
       content,
       teacher_id,
       teacher_name,
       job_number,
       classroom_id,
       classroom_name,
       is_outline,
       class_mode,
       is_stage_exam,
       is_pay,
       tutor_teacher_id,
       tutor_teacher_name,
       tutor_job_number,
       is_subsidy,
       answer_teacher_id,
       answer_teacher_name,
       answer_job_number,
       remark,
       create_time
from onl_edu_ods.course_table_upload_detail;