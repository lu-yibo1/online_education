-- create database if not exists onl_edu_dwd;
-- create database if not exists onl_edu_dwb;
-- create database if not exists onl_edu_dws;

--维度表：在读学员人数信息表
drop table if exists onl_edu_dwd.dt_class_studying_student_count;
create table onl_edu_dwd.dt_class_studying_student_count(
    id                     int,
    school_id              int comment '校区id',
    subject_id             int comment '学科id',
    class_id               int comment '班级id',
    studying_student_count int comment '在读班级人数',
    studying_date          string comment '在读日期'
)
comment '在读学员人数信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='snappy');

--维度表：班级排课信息表
drop table if exists onl_edu_dwd.dt_course_table_upload_detail;
create table onl_edu_dwd.dt_course_table_upload_detail(
    id                  int comment 'id',
    base_id             int comment '课程主表id',
    class_id            int comment '班级id',
    class_date          string comment '上课日期',
    content             string comment '课程内容',
    teacher_id          int comment '老师id',
    teacher_name        string comment '老师名字',
    job_number          string comment '工号',
    classroom_id        int comment '教室id',
    classroom_name      string comment '教室名称',
    is_outline          int comment '是否大纲 0 否 1 是',
    class_mode          int comment '上课模式 0 传统全天 1 AB上午 2 AB下午 3 线上直播',
    is_stage_exam       int comment '是否阶段考试（0：否 1：是）',
    is_pay              int comment '代课费（0：无 1：有）',
    tutor_teacher_id    int comment '晚自习辅导老师id',
    tutor_teacher_name  string comment '辅导老师姓名',
    tutor_job_number    string comment '晚自习辅导老师工号',
    is_subsidy          int comment '晚自习补贴（0：无 1：有）',
    answer_teacher_id   int comment '答疑老师id',
    answer_teacher_name string comment '答疑老师姓名',
    answer_job_number   string comment '答疑老师工号',
    remark              string comment '备注',
    create_time         string comment '创建时间'
)
comment '班级排课信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='snappy');