-- dm层插入数据
insert into hive.onl_edu_dm.dm_relationship
with t as (
    select
        dt,
        substring(dt,1, 4) as year_code,
        substring(dt,1,7) as year_month,
        customer_type,
        clue_state,
        origin_channel,
        area,
        sc_name,
        sj_name,
        dp_name,
        group_type,
        customer_cnt
    from hive.onl_edu_dws.dws_relationship_daycount
), groupby as
(select
    dt,
    year_code,
    year_month,
    case when grouping(year_code,year_month,dt)=0
        then 'day'
        when grouping(year_code,year_month,dt)=1
        then 'month'
        when grouping(year_code,year_month,dt)=3
        then 'year'
        else 'others'
        end
        as time_type,
    customer_type,
    clue_state,
    origin_channel,
    area,
    sc_name,
    sj_name,
    dp_name,
    case when grouping(area)=0
        then 'area'
        when grouping(sc_name)=0
        then 'school'
        when grouping(sj_name)=0
        then 'subject'
        when grouping(origin_channel)=0
        then 'origin_channel'
        when grouping(dp_name)=0
        then 'department'
        else 'all'
        end
    as new_group_type,
    case when grouping(customer_type,clue_state,area,sc_name,sj_name,origin_channel,dp_name)=15 and group_type = 'area'
        then sum(customer_cnt)
        when grouping(customer_type,clue_state,area,sc_name,sj_name,origin_channel,dp_name)=23 and group_type = 'school'
        then sum(customer_cnt)
        when grouping(customer_type,clue_state,area,sc_name,sj_name,origin_channel,dp_name)=27 and group_type = 'subject'
        then sum(customer_cnt)
        when grouping(customer_type,clue_state,area,sc_name,sj_name,origin_channel,dp_name)=29 and group_type = 'origin_channel'
        then sum(customer_cnt)
        when grouping(customer_type,clue_state,area,sc_name,sj_name,origin_channel,dp_name)=30 and group_type = 'department'
        then sum(customer_cnt)
        when grouping(customer_type,clue_state,area,sc_name,sj_name,origin_channel,dp_name)=31 and group_type = 'all'
        then sum(customer_cnt)
        else null
        end
    as customer_cnt
from t
group by
grouping sets (
-- 年
    (year_code,customer_type,clue_state, group_type),
    (year_code,customer_type,clue_state,area, group_type),
    (year_code,customer_type,clue_state,sc_name, group_type),
    (year_code,customer_type,clue_state,sj_name, group_type),
    (year_code,customer_type,clue_state,origin_channel, group_type),
    (year_code,customer_type,clue_state,dp_name, group_type),
-- 月
    (year_code, year_month,customer_type,clue_state, group_type),
    (year_code, year_month, customer_type, clue_state, area, group_type),
    (year_code, year_month, customer_type, clue_state, sc_name, group_type),
    (year_code, year_month, customer_type, clue_state, sj_name, group_type),
    (year_code, year_month, customer_type, clue_state, origin_channel, group_type),
    (year_code, year_month, customer_type, clue_state, dp_name, group_type),
-- 天
    (year_code, year_month,dt,customer_type,clue_state, group_type),
    (year_code, year_month,dt, customer_type, clue_state, area, group_type),
    (year_code, year_month,dt, customer_type, clue_state, sc_name, group_type),
    (year_code, year_month,dt, customer_type, clue_state, sj_name, group_type),
    (year_code, year_month,dt, customer_type, clue_state, origin_channel, group_type),
    (year_code, year_month,dt, customer_type, clue_state, dp_name, group_type)

    ))
select
    dt,
    year_code,
    year_month,
    time_type,
    customer_type,
    clue_state,
    origin_channel,
    area,
    sc_name,
    sj_name,
    dp_name,
    new_group_type as group_type,
    customer_cnt
from groupby
where customer_cnt is not null;

insert into hive.onl_edu_dm.dm_clue_appeal
with t1 as
(select
    substring(dt, 1, 10) as dt,
       substring(dt, 12,2) as hour_code,
       clue_state,
       customer_type,
       cast(count(if(appeal_status = 2, customer_id, null)) as decimal(5,2))/ if(count(customer_id)>0, count(customer_id), 1) as appeal_valid_rate
from hive.onl_edu_dwb.dwb_clue_appeal_detail
group by substring(dt, 1, 10), substring(dt, 12,2),clue_state, customer_type), t2 as
(select
    dt,
    substring(dt,1,4) as year_code,
    substring(dt,1,7) as year_month,
    clue_state,
    customer_type,
    valid_appeal_cnt,
    appeal_all_cnt
from hive.onl_edu_dws.dws_clue_appeal_daycount), t3 as
(select
    dt,
    year_code,
    year_month,
    case when grouping (dt) = 0
        then 'day'
        when grouping(year_month) = 0
        then 'month'
        when grouping(year_code) = 0
        then 'year'
        else 'other'
        end
        as time_type,
    clue_state,
    customer_type,
    sum(valid_appeal_cnt) as valid_appeal_cnt
from t2
group by
grouping sets (
--年
(year_code,clue_state,customer_type),
--月
(year_code,year_month,clue_state,customer_type),
--日
(year_code,year_month,dt,clue_state,customer_type)
    ))
select
    t3.dt,
    t1.hour_code,
    t3.year_code,
    t3.year_month,
    t3.time_type,
    t3.clue_state,
    t3.customer_type,
    t3.valid_appeal_cnt,
    t1.appeal_valid_rate
from t3 full join t1 on t3.dt = t1.dt;

insert into onl_edu_dm.dm_signup
select
       '2023-05-31' as date_time,
       case when grouping(year_code,month_code,create_time)=0
           then '天'
           when grouping(year_code,month_code)=0
           then '月'
           when grouping(year_code)=0
           then '年'
            else 'other'  end as time_type,
        year_code    ,
        case when grouping(year_month)=0
            then year_month
            else null end  year_month,
        month_code  ,
        create_time,
        origin_channel,
        itcast_school_name  ,
        itcast_subject_name  ,
        origin_type,
        case when grouping(origin_type,itcast_school_name,itcast_subject_name)=0
            then '线上线下+校区+学科'
        when grouping(origin_type,itcast_subject_name)=0
            then '线上线下+学科'
        when grouping(origin_type,itcast_school_name)=0
            then '线上线下+校区'
        when grouping(itcast_school_name)=0
            then '校区'
        when grouping(origin_type,origin_channel)=0
            then '线上线下+来源渠道'
--         when grouping(origin_type,creator,creator_depart_id,creator_depart_name)=0
        when grouping(origin_type,creator_depart_name)=0
            then '线上线下+咨询中心'
        else 'other' end  as group_type,
        creator_depart_name  ,

        sum(signup_cnt) as signup_cnt,
       sum(potential_cnt) as potential_cnt,
       sum(clue_cnt) as clue_cnt,
        null as potential_to_signup_percent ,
        null as clue_to_signup_percent
from onl_edu_dws.dws_signup_daycount
group by
grouping sets (
    -- 年
        (year_code),
        (year_code,itcast_school_name),
        (year_code,origin_type,itcast_school_name),
        (year_code,origin_type,itcast_subject_name),
        (year_code,origin_type,itcast_school_name,itcast_subject_name),
        (year_code,origin_type,origin_channel),
        (year_code,origin_type,creator_depart_name),
    -- 月
        (year_code,year_month,month_code),
        (year_code,year_month,month_code,itcast_school_name),
        (year_code,year_month,month_code,origin_type,itcast_school_name),
        (year_code,year_month,month_code,origin_type,itcast_subject_name),
        (year_code,year_month,month_code,origin_type,itcast_school_name,itcast_subject_name),
        (year_code,year_month,month_code,origin_type,origin_channel),
        (year_code,year_month,month_code,origin_type,creator_depart_name),

  -- 日
        (year_code,year_month,month_code,create_time),
        (year_code,year_month,month_code,create_time,itcast_school_name),
        (year_code,year_month,month_code,create_time,origin_type,itcast_school_name),
        (year_code,year_month,month_code,create_time,origin_type,itcast_subject_name),
        (year_code,year_month,month_code,create_time,origin_type,itcast_school_name,itcast_subject_name),
        (year_code,year_month,month_code,create_time,origin_type,origin_channel),
        (year_code,year_month,month_code,create_time,origin_type,creator_depart_name)
    )
;

-- 考勤签到统计表 left join 请假统计表 left join 在读学员人数信息表
-- 旷课人数及旷课率计算
-- 整合所有需求所要的指标
insert into onl_edu_dm.dm_attendance
with tmp as (
 select  distinct   sc.day,
                    sc.class_id,
                    sc.period,
                    dc.studying_student_count,
                    sc.normal_signin_cnt,
                    cast(sc.attendance_ratio as decimal (7,4)) as attendance_ratio,
                    sc.late_signin_cnt,
                    cast(sc.late_attend_ratio as decimal (7,4)) as late_attend_ratio,
                    coalesce(lc.leave_cnt, 0)                                                                     as leave_cnt,
                    cast(coalesce(lc.leave_ratio,0) as decimal (7,4) )                                                                as leave_ratio,
                    coalesce(dc.studying_student_count - sc.normal_signin_cnt - sc.late_signin_cnt - lc.leave_cnt,
                             0)                                                                                       as play_hooky_cnt,
                    coalesce(dc.studying_student_count - sc.normal_signin_cnt - sc.late_signin_cnt - lc.leave_cnt,
                                  0)
                        /
                         cast(dc.studying_student_count as decimal(38, 10))                          as play_hooky_ratio
    from hive.onl_edu_dws.dws_signin_cnt sc
             left join hive.onl_edu_dws.dws_leave_cnt lc
                       on sc.day = lc.day and sc.class_id = lc.class_id and sc.period = lc.period
             left join hive.onl_edu_dwd.dt_class_studying_student_count dc
                       on sc.day = dc.studying_date and sc.class_id = dc.class_id

)
  select
                    tmp.day,
                    tmp.class_id,
                    tmp.period,
                    tmp.normal_signin_cnt,
                    tmp.attendance_ratio,
                    tmp.late_signin_cnt,
                    tmp.late_attend_ratio,
                    tmp.leave_cnt,
                    tmp.leave_ratio,
                    tmp.play_hooky_cnt,
                    cast(tmp.play_hooky_ratio as decimal (7,4)) as play_hooky_ratio
from tmp
order by tmp.day, tmp.class_id,
             case
                 when tmp.period = 'morning' then 1
                 when tmp.period = 'afternoon' then 2
                 when tmp.period = 'evening' then 3
                 end asc;
