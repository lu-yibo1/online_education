drop table if exists onl_edu_dwb.dwb_signin_detail;
create table onl_edu_dwb.dwb_signin_detail(
     id                int comment '主键id',
    time_table_id     int comment '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id          int comment '班级id',
    student_id        int comment '学员id',
    signin_time       string comment '签到时间',
    signin_date       string comment '签到日期',
--在读学员人数信息表
    studying_student_count int comment '在读班级人数',
    studying_date          string comment '在读日期',
--班级排课信息表
    class_date          string comment '上课日期',
    content             string comment '课程内容',
--班级作息时间表
    morning_template_id   int comment '上午出勤模板',
    morning_begin_time    string comment '上午开始时间',
    morning_end_time      string comment '上午结束时间',
    afternoon_template_id string comment '下午出勤模板id',
    afternoon_begin_time  string comment '下午开始时间',
    afternoon_end_time    string comment '下午结束时间',
    evening_template_id   int comment '晚上出勤模板id',
    evening_begin_time    string comment '晚上开始时间',
    evening_end_time      string comment '晚上结束时间',
    use_begin_date        string comment '使用开始日期',
    use_end_date          string comment '使用结束日期'
)
comment '考勤分析签到宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='snappy');


drop table if exists onl_edu_dwb.dwb_leave_detail;
create table onl_edu_dwb.dwb_leave_detail(
    id              int comment '序列id',
    class_id        int comment '班级id',
    student_id      int comment '学员id',
    audit_state     int comment '审核状态 0 待审核 1 通过 2 不通过',
    leave_type      int comment '请假类型  1 请假 2 销假',
    begin_time      string comment '请假开始时间',
    begin_time_type int comment '1：上午 2：下午',
    end_time        string comment '请假结束时间',
    end_time_type   int comment '1：上午 2：下午',
    days            decimal(5, 1) comment '请假/已休天数',
    cancel_state    int comment '撤销状态  0 未撤销 1 已撤销',
    cancel_time     string comment '撤销时间',
    valid_state     int comment '是否有效（0：无效 1：有效）',
--在读学员人数信息表
    studying_student_count int comment '在读班级人数',
    studying_date          string comment '在读日期',
--班级排课信息表
    class_date          string comment '上课日期',
    content             string comment '课程内容',
--班级作息时间表
    morning_template_id   int comment '上午出勤模板',
    morning_begin_time    string comment '上午开始时间',
    morning_end_time      string comment '上午结束时间',
    afternoon_template_id string comment '下午出勤模板id',
    afternoon_begin_time  string comment '下午开始时间',
    afternoon_end_time    string comment '下午结束时间',
    evening_template_id   int comment '晚上出勤模板id',
    evening_begin_time    string comment '晚上开始时间',
    evening_end_time      string comment '晚上结束时间',
    use_begin_date        string comment '使用开始日期',
    use_end_date          string comment '使用结束日期'
)
comment '考勤分析请假宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='snappy');