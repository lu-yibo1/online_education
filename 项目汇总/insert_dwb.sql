
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;


-- 优化测试  GC overhead limit exceeded
--   set mapreduce.map.memory.mb=6144;
--   set mapreduce.map.java.opts=-Xmx4608M;（根据需要更改具体值，但这对参数的比例是1:0.75）
--   set mapreduce.reduce.memory.mb = 8192;
--   set mapreduce.reduce.java.opts = -Xmx6144M;（根据需要更改具体值，但这对参数的比例是1:0.75）

set mapred.child.java.opts=-Xmx3000m;
set mapreduce.map.java.opts=-Xmx2096m; -- （根据需要更改具体值，但这对参数的比例是1:0.75）
set mapreduce.reduce.java.opts=-Xmx2096m; -- （根据需要更改具体值，但这对参数的比例是1:0.75）
set mapreduce.map.memory.mb=1096;
set mapreduce.reduce.memory.mb=1096;

-- 提升vcore的数量，加快container的计算，降低oom的几率
  set mapreduce.map.cpu.vcores = 4;
  set mapreduce.reduce.cpu.vcores = 4;

  set mapreduce.input.fileinputformat.split.maxsize = 256000000;
  set mapreduce.input.fileinputformat.split.minsize = 256000000;

-- set mapreduce.map.java.opts=-Xmx4096m -XX:+UseConcMarkSweepGC;
set hive.groupby.skewindata=true;  -- Map阶段groupby阶段防倾斜
set hive.optimize.skewjoin=true;  -- join阶段防出现数据倾斜
set hive.auto.convert.join=false;  --关闭mapjoin
-- set hive.groupby.skewindata=true;  --生成的查询计划会有两个MRJob。
set mapred.reduce.tasks=4;

insert into hive.onl_edu_dwb.dwb_customer_relationship_detail
select distinct
    substring(a.create_date_time,1,10) as dt,
    a.customer_id,
    if(a.origin_type = 'NETSERVICE', 1, 0) as customer_type,
    a.origin_channel,
    b.area,
    if(c.clue_state = 'VALID_NEW_CLUES', 1, 0) as clue_state,
    d.name as sc_name,
    e.name as sj_name,
    f.tdepart_id,
    g.name as dp_name
from hive.onl_edu_dwd.ft_customer_relationship a
left join hive.onl_edu_dwd.dt_customer b on a.customer_id = b.id
left join hive.onl_edu_dwd.dt_customer_clue c on a.customer_id = c.customer_relationship_id
left join hive.onl_edu_dwd.dt_itcast_school d on a.itcast_school_id = d.id
left join hive.onl_edu_dwd.dt_itcast_subject e on a.itcast_subject_id = e.id
left join hive.onl_edu_dwd.dt_employee f on a.creator = f.id
left join hive.onl_edu_dwd.dt_scrm_department g on f.tdepart_id = g.id;

insert into hive.onl_edu_dwb.dwb_clue_appeal_detail
select distinct
    a.create_date_time as dt,
    a.customer_relationship_id,
    if(a.clue_state = 'VALID_NEW_CLUES', 1, 0) as clue_state,
    if(c.origin_type = 'NETSERVICE', 1, 0) as customer_type,
    b.appeal_status
from hive.onl_edu_dwd.ft_customer_clue a
left join hive.onl_edu_dwd.dt_customer_appeal b on a.customer_relationship_id = b.customer_relationship_first_id
left join hive.onl_edu_dwd.ft_customer_relationship c on a.customer_relationship_id = c.customer_id;

insert into onl_edu_dwb.dwb_signup_detail partition (dt)
select
    cr.id,
    cr.payment_state,
    cr.create_date_time,
    cr.origin_type,
    cr.itcast_clazz_id,
    cr.creator,
    cr.origin_channel,
    cr.first_id,
    ca.customer_relationship_first_id,
    ca.appeal_status,
    ic.itcast_school_id,
    ic.itcast_school_name,
    ic.itcast_subject_id,
    ic.itcast_subject_name,
    em.tdepart_id as creator_depart_id,
    sd.name as creator_depart_name,
    '2023-05-31'as dt
from onl_edu_dwd.ft_customer_relationship cr
left join onl_edu_dwd.dt_customer_appeal ca on cr.first_id = ca.id
left join onl_edu_dwd.dt_itcast_clazz ic on cr.itcast_clazz_id = ic.id
left join onl_edu_dwd.dt_employee em on cr.creator = em.id
left join onl_edu_dwd.dt_scrm_department sd on em.tdepart_id = sd.id;
-- where cr.payment_state = 'PAID';

insert into hive.onl_edu_dwb.dwb_signin_detail
select fr.id,
       -- normal_class_flag,
       fr.time_table_id,
       fr.class_id,
       fr.student_id,
       fr.signin_time,
       fr.signin_date,
       fr.inner_flag,
       fr.signin_type,
       --dc.class_id,
       dc.studying_student_count,
       dc.studying_date,
        dd.base_id,
        dd.class_date,
        dd.content,
         dt.morning_template_id,
         dt.morning_begin_time,
         dt.morning_end_time,
         dt.afternoon_template_id,
         dt.afternoon_begin_time,
         dt.afternoon_end_time,
         dt.evening_template_id,
         dt.evening_begin_time,
         dt.evening_end_time,
         dt.use_begin_date,
         dt.use_end_date

from hive.onl_edu_dwd.ft_tbh_student_signin_record fr
join hive.onl_edu_dwd.dt_course_table_upload_detail dd
on fr.class_id = dd.class_id and fr.signin_date = dd.class_date
join hive.onl_edu_dwd.dt_class_studying_student_count dc
on dd.class_id = dc.class_id and dd.class_date = dc.studying_date
join hive.onl_edu_dwd.dt_tbh_class_time_table dt
on fr.class_id = dt.class_id and dd.class_date >= dt.use_begin_date and dd.class_date <= dt.use_end_date;

insert into hive.onl_edu_dwb.dwb_leave_detail
select
       fa.id,
       fa.class_id,
       fa.student_id,
       fa.audit_state,
       fa.leave_type,
       fa.begin_time,
       fa.begin_time_type,
       fa.end_time,
       fa.end_time_type,
       fa.days,
       fa.cancel_state,
       fa.cancel_time,
       fa.valid_state,
       --dc.class_id,
       dc.studying_student_count,
       dc.studying_date,
        dd.class_date,
        dd.content,
        dt.morning_template_id,
        dt.morning_begin_time,
        dt.morning_end_time,
        dt.afternoon_template_id,
        dt.afternoon_begin_time,
        dt.afternoon_end_time,
        dt.evening_template_id,
        dt.evening_begin_time,
        dt.evening_end_time,
        dt.use_begin_date,
        dt.use_end_date

from hive.onl_edu_dwd.ft_student_leave_apply fa
join hive.onl_edu_dwd.dt_course_table_upload_detail dd
on fa.class_id = dd.class_id and dd.class_date >= substr(fa.begin_time,1,10)
and dd.class_date <= substr(fa.end_time,1,10)
join hive.onl_edu_dwd.dt_class_studying_student_count dc
on dd.class_id = dc.class_id and dd.class_date = dc.studying_date
join hive.onl_edu_dwd.dt_tbh_class_time_table dt
on fa.class_id = dt.class_id and dd.class_date >= dt.use_begin_date and dd.class_date <= dt.use_end_date;




