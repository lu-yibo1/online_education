insert into hive.onl_edu_dws.dws_relationship_daycount
select
    -- 维度
    dt,
    customer_type ,
    clue_state,
    origin_channel,
    area,
    sc_name,
    sj_name,
    dp_name,
   -- 分组类型
    case when grouping(area) = 0
        then 'area'
        when grouping(sc_name) = 0
        then 'school'
        when grouping(sj_name) = 0
        then 'subject'
        when grouping(origin_channel) = 0
        then 'origin_channel'
        when grouping(dp_name) = 0
        then 'department'
        else 'all'
        end
       as group_type,
       -- 计算
     count(customer_id) as customer_cnt
from hive.onl_edu_dwb.dwb_customer_relationship_detail
group by
grouping sets (
    (dt,customer_type,clue_state),
    (dt,customer_type,clue_state,area),
    (dt,customer_type,clue_state,sc_name),
    (dt,customer_type,clue_state,sj_name),
    (dt,customer_type,clue_state,origin_channel),
    (dt,customer_type,clue_state,dp_name)
    );


insert into hive.onl_edu_dws.dws_clue_appeal_daycount
select
    substring(dt, 1, 10) as dt,
       clue_state,
       customer_type,
       count(if(appeal_status = 2, customer_id, null)) as valid_appeal_cnt,
       count(customer_id) as appeal_all_cnt,
       cast(count(if(appeal_status = 2, customer_id, null)) as decimal(5,2))/ if(count(customer_id)>0, count(customer_id), 1) as appeal_valid_rate
from hive.onl_edu_dwb.dwb_clue_appeal_detail
group by substring(dt, 1, 10), clue_state, customer_type;
select * from onl_edu_dws.dws_relationship_daycount where dt = '2015-04-27' and group_type = 'all';



select count(*) from onl_edu_dwb.dwb_signup_detail;


-- select * from onl_edu_dwb.dwb_signup_detail;
-- select count(*) from onl_edu_dwb.dwb_signup_detail;

--主题目宽表
insert into onl_edu_dws.dws_signup_daycount
with  tmp  as (
 select
    substring(dt,1,4) as  year_code,
    substring(dt,1,7)as year_month,
    substring(dt,6,2) as  month_code,
    dt as create_time,
-- 维度
    case when grouping(origin_type,itcast_school_id,itcast_subject_id)=0
            then '线上线下+校区+学科'
        when grouping(origin_type,itcast_subject_id)=0
            then '线上线下+学科'
        when grouping(origin_type,itcast_school_id)=0
            then '线上线下+校区'
        when grouping(itcast_school_id)=0
            then '校区'
        when grouping(origin_type,origin_channel)=0
            then '线上线下+来源渠道'
--         when grouping(origin_type,creator,creator_depart_id,creator_depart_name)=0
        when grouping(origin_type,creator_depart_name)=0
            then '线上线下+咨询中心'
        else 'other' end  as group_type,
    -- 线上线下
    case when grouping(origin_type)=0
        then if(origin_type = 'NETSERVICE','线上','线下')
        else null
        end  as origin_type,
    -- 校区
    case when grouping(itcast_school_id) = 0
        then  itcast_school_id
		else null end as itcast_school_id ,

    case when grouping(itcast_school_name) = 0
        then  itcast_school_name
		else null end as itcast_school_name ,
--     -- 学科
    case when grouping(itcast_subject_id) = 0
        then  itcast_subject_id
		else null end as itcast_subject_id ,
    case when grouping(itcast_subject_name) = 0
        then  itcast_subject_name
		else null end as itcast_subject_name ,

--     -- 来源渠道
    case when grouping(origin_channel) = 0
        then  if( origin_channel is not null,origin_channel,'-')
		else null end as origin_channel ,

    -- 咨询中心
--     case when grouping(creator) = 0
--         then  creator
-- 		else null end as creator ,

    case when grouping(creator_depart_id) = 0
        then  creator_depart_id
		else null end as creator_depart_id ,

    case when grouping(creator_depart_name) = 0
        then  creator_depart_name
		else null end as creator_depart_name ,


-- 指标
    -- 报名人数
    case when grouping(dt) = 0
        then  sum(if(payment_state='PAID',1,0))
		else null end as signup_cnt ,

    --意向人数
    case when grouping(dt) = 0
        then  sum(1)   --所有的都是意向人数
		else null end as potential_cnt ,

     -- 有效线索人数
     case when grouping(dt) = 0
        then  sum(if(appeal_status=2,1,0))   --所有的都是意向人数
		else null end as clue_cnt ,

     -- 意向转报名率
        null as potential_to_signup_percent,

     -- 有效线索报名转换率
        null as clue_to_signup_percent,
    '2023--5-31' as dt

from onl_edu_dwb.dwb_signup_detail
group by
grouping sets(
    dt, -- 时间
--     (dt,origin_type), -- 时间
    (dt,itcast_school_id,itcast_school_name), -- 时间+校区
    (dt,origin_type,itcast_school_id,itcast_school_name), -- 时间+线上线下+校区
    (dt,origin_type,itcast_subject_id,itcast_subject_name), -- 时间+线上线下+学科
    (dt,origin_type,itcast_school_id,itcast_school_name,itcast_subject_id,itcast_subject_name), -- 时间+线上线下+校区+学科

    (dt,origin_type,origin_channel), -- 时间+线上线下+来源渠道
--     (dt,origin_type,creator,creator_depart_id,creator_depart_name) -- 时间+线上线下+咨询中心
    (dt,origin_type,creator_depart_id,creator_depart_name) -- 时间+线上线下+咨询中心
)
)
-- select group_type, count(*) from tmp group by  group_type;
select year_code,
       year_month,
       month_code,
       create_time,
       group_type,
       origin_type,
       itcast_school_id,
       itcast_school_name,
       itcast_subject_id,
       itcast_subject_name,
       origin_channel,
       creator_depart_id,
       creator_depart_name,
       signup_cnt,
       potential_cnt,
       clue_cnt,
       if(potential_cnt=0 or signup_cnt=0,0, signup_cnt/(potential_cnt*1.00) ) as potential_to_signup_percent,
       if(clue_cnt=0 or signup_cnt=0,0, signup_cnt/(clue_cnt*1.00) )  as clue_to_signup_percent,
       dt
from tmp
;

select * from onl_edu_dwb.dwb_signup_detail
--          where creator is null
--            where creator_depart_id is null
           where creator_depart_name is null
;


insert into hive.onl_edu_dws.dws_signin_cnt
with tmp as (
--字段提取及数值转换便于计算
    select student_id,
           class_id,
           studying_student_count,
           signin_date,
           signin_time                                               as time1,
           signin_time                                               as time2,
           cast(morning_end_time as time)                            as end_mon,
           cast(afternoon_end_time as time)                          as end_aft,
           cast(evening_end_time as time)                            as end_eve,
           cast(morning_begin_time as time) - interval '40' minute   as upper_limit_mon,
           cast(morning_begin_time as time) + interval '10' minute   as lower_limit_mon,
           cast(afternoon_begin_time as time) - interval '40' minute as upper_limit_aft,
           cast(afternoon_begin_time as time) + interval '10' minute as lower_limit_aft,
           cast(evening_begin_time as time) - interval '40' minute   as upper_limit_eve,
           cast(evening_begin_time as time) + interval '10' minute   as lower_limit_eve,
           cast(substring(signin_time, 12, 19) as time)              as s_time
    from hive.onl_edu_dwb.dwb_signin_detail
),
s1 as (     select signin_date,
            class_id,
            (case
                when s_time between upper_limit_mon and end_mon then 'morning'
                when s_time between upper_limit_aft and end_aft then 'afternoon'
                when s_time between upper_limit_eve and end_eve then 'evening'
               end)       as period,
-- 添加时间段作为列：上午 、 下午 、 晚上
--正常出勤人数
     count(distinct
                 case
                     when s_time between upper_limit_mon and lower_limit_mon then student_id
                     when s_time between upper_limit_aft and lower_limit_aft then student_id
                     when s_time between upper_limit_eve and lower_limit_eve then student_id
                     end)                                                 as normal_signin_cnt,
     count(distinct
                 case
                     when s_time between upper_limit_mon and lower_limit_mon then student_id
                     when s_time between upper_limit_aft and lower_limit_aft then student_id
                     when s_time between upper_limit_eve and lower_limit_eve then student_id
                     end) / cast(studying_student_count as decimal(38, 5))
                     as attendance_ratio,
-- 找出正常打卡学生的id
--             (select distinct student_id
--              from tmp
--              where student_id = (case when s_time between upper_limit_mon and lower_limit_mon then student_id end)
--             ) as student_mon,
--
--             (select distinct student_id
--              from tmp
--              where student_id = (case when s_time between upper_limit_aft and lower_limit_aft then student_id end)
--             ) as student_aft,
--
--             (  select distinct student_id
--               from tmp
--               where student_id = (case when s_time between upper_limit_eve and lower_limit_eve then student_id end)
--             ) as student_eve,

-- 迟到学生按时间段判断，同时去重，以及去除正常打卡学生（正常打卡学生也可能在迟到时间内打卡）
     count(distinct
             case
                 when (s_time > lower_limit_mon and s_time <= end_mon and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_mon and lower_limit_mon
                                                 then student_id end)
                 )) then student_id
                 when (s_time > lower_limit_aft and s_time <= end_aft and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_aft and lower_limit_aft
                                                 then student_id end)
                 )
                     ) then student_id
                 when (s_time > lower_limit_eve and s_time <= end_eve and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_eve and lower_limit_eve
                                                 then student_id end)
                 )
                     ) then student_id
                 end) as late_signin_cnt,
    count(distinct
             case
                 when (s_time > lower_limit_mon and s_time <= end_mon and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_mon and lower_limit_mon
                                                 then student_id end)
                 )) then student_id
                 when (s_time > lower_limit_aft and s_time <= end_aft and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_aft and lower_limit_aft
                                                 then student_id end)
                 )
                     ) then student_id
                 when (s_time > lower_limit_eve and s_time <= end_eve and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_eve and lower_limit_eve
                                                 then student_id end)
                 )
                     ) then student_id
                 end) / cast(studying_student_count as decimal(38, 5)) as late_attend_ratio
from tmp
group by signin_date, class_id, studying_student_count,
     case when s_time between upper_limit_mon and end_mon then 'morning'
             when s_time between upper_limit_aft and end_aft then 'afternoon'
             when s_time between upper_limit_eve and end_eve then 'evening'
             end
order by signin_date)

select  s1.signin_date,
        s1.class_id,
        s1.period,
        s1.normal_signin_cnt,
        s1.attendance_ratio,
        s1.late_signin_cnt,
        s1.late_attend_ratio
from s1
where period is not null -- 去除period为null的情况
order by signin_date,
case period
           when 'morning' then 1
           when 'afternoon' then 2
           else 3
         end asc;


--grouping sets
insert into hive.onl_edu_dws.dws_leave_cnt
with tmp as (
    select class_date,
           class_id,
           student_id,
           audit_state,
           studying_student_count,
           cancel_state,
           valid_state,
           cast(substring(begin_time, 12, 19) as time) as l_beg_time,
           cast(substring(end_time, 12, 19) as time)   as l_end_time,
           cast(morning_begin_time as time)            as begin_mon,
           cast(afternoon_begin_time as time)          as begin_aft,
           cast(evening_begin_time as time)            as begin_eve,
           cast(morning_end_time as time)              as end_mon,
           cast(afternoon_end_time as time)            as end_aft,
           cast(evening_end_time as time)              as end_eve,
           'morning'                                   as am,
           'afternoon'                                 as pm,
           'evening'                                   as eve
    from hive.onl_edu_dwb.dwb_leave_detail
)
select class_date,
       class_id,
       case
           when grouping(class_date, class_id, studying_student_count, am) = 0 then 'morning'
           when grouping(class_date, class_id, studying_student_count, pm) = 0 then 'afternoon'
           when grouping(class_date, class_id, studying_student_count, eve) = 0 then 'evening'
           end as period,

       case
           when grouping(class_date, class_id, studying_student_count, am) = 0
               then count(distinct if(begin_mon >= l_beg_time and end_mon <= l_end_time, student_id, null))
           when grouping(class_date, class_id, studying_student_count, pm) = 0
               then count(distinct if(begin_aft >= l_beg_time and end_aft <= l_end_time, student_id, null))
           when grouping(class_date, class_id, studying_student_count, eve) = 0
               then count(distinct if(begin_eve >= l_beg_time and end_eve <= l_end_time, student_id, null))
           end as leave_cnt,
--            count(case
--                 when begin_mon >= l_beg_time and end_mon  <= l_end_time then student_id
--                 when begin_aft >= l_beg_time and end_aft  <= l_end_time then student_id
--                 when begin_eve >= l_beg_time and end_eve  <= l_end_time then student_id
--                end) as leave_cnt
       case
           when grouping(class_date, class_id, studying_student_count, am) = 0
               then
                   count(distinct if(begin_mon >= l_beg_time and end_mon <= l_end_time, student_id, null))
                                 / cast(studying_student_count as decimal(38, 5))
           when grouping(class_date, class_id, studying_student_count, pm) = 0
               then
                   count(distinct if(begin_aft >= l_beg_time and end_aft <= l_end_time, student_id, null))
                                 / cast(studying_student_count as decimal(38, 5))
           when grouping(class_date, class_id, studying_student_count, eve) = 0
               then
                   count(distinct if(begin_eve >= l_beg_time and end_eve <= l_end_time, student_id, null)
                                 ) / cast(studying_student_count as decimal(38, 5))
           end as leave_ratio
from tmp
where audit_state = 1
  and cancel_state = 0
  and valid_state = 1
group by
    grouping sets ( (class_date, class_id, studying_student_count, am),
                    (class_date, class_id, studying_student_count, pm),
                    (class_date, class_id, studying_student_count, eve)
    )
order by
case when period = 'morning' then 1
when period = 'afternoon' then 2
when period = 'evening' then 3
end asc;


