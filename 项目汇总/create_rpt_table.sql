--rpt层建表语句
-- 需求1：总意向量（线型图）
create table if not exists onl_edu_rpt.rpt_relationship_all_cnt(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    customer_cnt          bigint        comment '意向用户个数'
)comment '总意向量表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

-- 需求2：意向学员位置热力图（地图热力图）
create table if not exists onl_edu_rpt.rpt_relationship_area_cnt(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    area                  string        comment '所在区域',
    customer_cnt          bigint        comment '意向用户个数'
)comment '各地区意向量表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

-- 需求3：意向学科排名（柱状图）
create table if not exists onl_edu_rpt.rpt_relationship_subject_cnt(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    sj_name               string        comment '学科名称',
    customer_cnt          bigint        comment '意向用户个数'
)comment '各学科意向量排名表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

-- 需求4：意向校区排名（柱状图）
create table if not exists onl_edu_rpt.rpt_relationship_school_cnt(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    sc_name               string        comment '校区名称',
    customer_cnt          bigint        comment '意向用户个数'
)comment '各校区意向量排名表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


-- 需求5：来源渠道占比（饼状图）
create table if not exists onl_edu_rpt.rpt_relationship_origin_rate(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    origin_channel               string        comment '来源渠道',
    customer_cnt          bigint        comment '意向用户占比'
)comment '各来源渠道意向量占比表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

-- 需求6：意向贡献中心占比（饼状图）
create table if not exists onl_edu_rpt.rpt_relationship_dp_rate(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    dp_name               string        comment '咨询中心',
    customer_cnt          bigint        comment '意向用户占比'
)comment '各中心意向量占比表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


-- 需求7：有效线索转化率
create table if not exists onl_edu_rpt.rpt_clue_valid_msgrate(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    valid_appeal_cnt        bigint        comment '有效线索个数',
    msg_valid_rate      decimal(5,2)   comment '访客咨询率'
)comment '访客线索有效转化率表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


-- 需求8：有效线索转化率
create table if not exists onl_edu_rpt.rpt_clue_valid_hrate(
    dt				string		   comment '创建时间（年月日）',
    hour_code       string		   comment '小时，例如20',
    clue_state               int    comment '线索状态：1-新客户、0-老客户',
    customer_type           int          comment '客户类型：1-线上、0-线下',
    appeal_valid_rate       decimal(5,2)    comment '有效线索转化率'
)comment '各时间段有效线索转化率表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

create database if not exists onl_edu_rpt;


-- 每天/每月/每年各个校区的报名人数
create table  mysql.onl_edu.school_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
itcast_school_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='校区';

-- 每天/每月/每年线上线下各个校区的报名人数
create table  mysql.onl_edu.origin_school_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
itcast_school_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+校区';

-- 每天/每月/每年线上线下各个学科的报名人数
create table  mysql.onl_edu.origin_subject_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
itcast_subject_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+学科';

-- 每天/每月/每年线上线下各个校区各个学科的报名人数
create table  mysql.onl_edu.origin_school_subject_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
itcast_school_name,
itcast_subject_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+校区+学科';

-- 每天/每月/每年线上线下各个来源渠道的报名人数
create table  mysql.onl_edu.origin_channel_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
origin_channel,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+来源渠道';

-- 每天/每月/每年线上线下各个咨询中心的报名人数
create table  mysql.onl_edu.origin_depart_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
creator_depart_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+咨询中心';


-- 每天/每月/每年线上线下的意向转报名率 = 报名人数 / 意向人数
create table  mysql.onl_edu.origin_potential_percent as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
if(potential_cnt=0 or signup_cnt=0,0, signup_cnt/(potential_cnt*1.00) ) as potential_to_signup_percent
from onl_edu_dm.dm_signup;

-- 每天/每月/每年线上的有效线索报名转换率 = 报名人数 / 有效线索人数
create table  mysql.onl_edu.origin_clue_percent as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
if(clue_cnt=0 or signup_cnt=0,0, signup_cnt/(clue_cnt*1.00) )  as clue_to_signup_percent
from onl_edu_dm.dm_signup;


-- RPT层  /每天/每班/    早中晚出勤人数、出勤率
create table hive.onl_edu_rpt.attendance_table as
select
       class_id,
       signin_date,
       morning_success_sign,
       afternoon_success_sign,
       evening_success_sign,
       morning_sign_ratio,
       afternoon_sign_ratio,
       evening_sign_artio
from onl_edu_dm.attendance_late_table;


-- /每天/每班   早中晚迟到人数、迟到率
create table  hive.onl_edu_rpt.late_table as
select class_id,
       signin_date,
       morning_late_sign,
       afternoon_late_sign,
       evening_late_sign,
       morning_late_ratio,
       afternoon_late_ratio,
       evening_late_ratio
from onl_edu_dm.attendance_late_table;

-- /每天/每班  早中晚请假人数、请假率
create table hive.onl_edu_rpt.leave_table as
select
       class_id,
       class_date,
       morning_leave_cnt,
       afternoon_leave_cnt,
       evening_leave_cnt,
       morning_leave_ratio,
       afternoon_leave_ratio,
       evening_leave_ratio
from hive.onl_edu_dm.leave_table;

-- /每天/每班  旷课人数、旷课率
create table hive.onl_edu_rpt.absent_table as
select * from hive.onl_edu_dm.absent_table;