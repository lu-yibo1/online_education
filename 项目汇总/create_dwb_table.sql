create table if not exists onl_edu_dwb.dwb_customer_relationship_detail(
    -- 原来的字段
    dt                              string        comment '创建时间（年月日）',
    customer_id                     int           comment '客户id',
    customer_type                   int          comment '客户类型：1-线上、0-线下',
    origin_type                     string        comment '来源渠道',
    -- 从学员信息表关联得到的字段
    area                     string   comment '所在区域',
    -- 从线索表关联得到的字段
    clue_state               int    comment '线索状态：1-新客户、0-老客户',
    --从校区信息表关联得到字段
    sc_name             string     comment '校区名称',
    --从学科信息表关联得到字段
    sj_name             string    comment '学科名称',
    -- 从员工信息表关联得到字段
    tdepart_id          int         comment '直属部门',
    --从部门信息表关联得到字段
    dp_name             string     comment '部门名称'
) comment '意向信息宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


create table onl_edu_dwb.dwb_clue_appeal_detail(
    -- 从意向表中关联得到dt
    dt                            string    comment '创建时间',
    --原来表的字段
    customer_id                   int       comment '客户id',
    clue_state                    int       comment '线索状态：1-新客户、0-老客户',
    customer_type                 int       comment '客户类型：1-线上、0-线下',
    appeal_status                 int       comment '申诉状态，0:待稽核 1:无效 2：有效'

) comment '线索信息宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');



-- 创建表
drop table if exists onl_edu_dwb.dwb_signup_detail;
create table onl_edu_dwb.dwb_signup_detail (
-- 意向信息表
    id int comment 'id',
    payment_state string comment '支付状态', -- paid 判断报名人数
    create_date_time string comment '创建时间',
    origin_type string comment '数据来源',
    itcast_clazz_id int comment '所属ems班级id',
    creator int comment '创建人',         -- 员工id 关联员工信息表
    origin_channel string comment '来源渠道',
    first_id int comment '第一条客户关系id',
-- 线索申诉信息表
    customer_relationship_first_id int comment '第一条客户关系id',
    appeal_status int comment '申诉状态，0:待稽核 1:无效 2：有效',  -- 判断有效线索人数 --只要在relation就是意向
-- 班级信息表
    itcast_school_id string comment 'ems校区ID',
    itcast_school_name string comment 'ems校区名称',
    itcast_subject_id string comment 'ems学科ID',
    itcast_subject_name string comment 'ems学科名称',
-- 员工信息表
    creator_depart_id int comment '创建人部门id',
-- 部门信息表
    creator_depart_name string comment '创建人部门名称'
)
comment '报名明细表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');



-- 创建表
drop table if exists onl_edu_dwb.dwb_signup_detail;
create table onl_edu_dwb.dwb_signup_detail (
-- 意向信息表
    id int comment 'id',
    payment_state string comment '支付状态', -- paid 判断报名人数
    create_date_time string comment '创建时间',
    origin_type string comment '数据来源',
    itcast_clazz_id int comment '所属ems班级id',
    creator int comment '创建人',         -- 员工id 关联员工信息表
    origin_channel string comment '来源渠道',
    first_id int comment '第一条客户关系id',
-- 线索申诉信息表
    customer_relationship_first_id int comment '第一条客户关系id',
    appeal_status int comment '申诉状态，0:待稽核 1:无效 2：有效',  -- 判断有效线索人数 --只要在relation就是意向
-- 班级信息表
    itcast_school_id string comment 'ems校区ID',
    itcast_school_name string comment 'ems校区名称',
    itcast_subject_id string comment 'ems学科ID',
    itcast_subject_name string comment 'ems学科名称',
-- 员工信息表
    creator_depart_id int comment '创建人部门id',
-- 部门信息表
    creator_depart_name string comment '创建人部门名称'
)
comment '报名明细表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');

-- 请假宽表
create table  hive.onl_edu_dwb.ft_student_leave_apply_wide as
select
            fa.id,
            fa.class_id,
            fa.student_id,
            fa.audit_state, -- 审核状态
            fa.leave_type,
            fa.begin_time,
            fa.begin_time_type,
            fa.end_time,
            fa.end_time_type,
            fa.days,
            fa.cancel_state,
            fa.valid_state,
        --  fa.create_time,  --扔了
         --   dd.class_id,
            dd.class_date,
            dd.content,
            dt.morning_template_id,  --这个没有
            dt.morning_begin_time,
            dt.morning_end_time,
            dt.afternoon_template_id,
            dt.afternoon_begin_time,
            dt.afternoon_end_time,
            dt.evening_template_id,
            dt.evening_begin_time,
            dt.evening_end_time,
            dt.use_begin_date,
            dt.use_end_date,
            dc.studying_student_count,
            dc.studying_date
from hive.onl_edu_dwd.ft_student_leave_apply fa
join hive.onl_edu_dwd.dt_course_table_upload_detail dd on fa.class_id=dd.class_id
and dd.class_date >=substring (fa.begin_time,1,10) and dd.class_date <= substring (fa.end_time,1,10)
join hive.onl_edu_dwd.dt_tbh_class_time_table dt on fa.class_id=dt.class_id
and dd.class_date>= dt.use_begin_date and dd.class_date <= dt.use_end_date
join hive.onl_edu_dwd.dt_class_studying_student_count dc on dd.class_id=dc.class_id
and dd.class_date=dc.studying_date;

-- 签到宽表
create table hive.onl_edu_dwb.ft_tbh_student_signin_record_wide as
select
       fr.id,
       fr.time_table_id,
       fr.class_id,
       fr.student_id,
       fr.signin_time,
       fr.signin_date,
       fr.inner_flag,
       fr.signin_type,
       --dc.class_id,
       dc.studying_student_count,
       dc.studying_date,
        -- dd.base_id, 课程id
        dd.class_date,
        dd.content,
         dt.morning_template_id,
         dt.morning_begin_time,
         dt.morning_end_time,
         dt.afternoon_template_id,
         dt.afternoon_begin_time,
         dt.afternoon_end_time,
         dt.evening_template_id,
         dt.evening_begin_time,
         dt.evening_end_time,
         dt.use_begin_date,
         dt.use_end_date
from onl_edu_dwd.ft_tbh_student_signin_record fr
    join onl_edu_dwd.dt_course_table_upload_detail dd
on fr.class_id = dd.class_id and fr.signin_date = dd.class_date
 join onl_edu_dwd.dt_class_studying_student_count dc
on fr.class_id = dc.class_id and fr.signin_date = dc.studying_date
 join onl_edu_dwd.dt_tbh_class_time_table dt
on fr.class_id = dt.class_id and dd.class_date >=dt.use_begin_date
and dd.class_date<=dt.use_end_date;