-- dm层建表
drop table if exists onl_edu_dm.dm_relationship;
create table if not exists onl_edu_dm.dm_relationship(

    dt				string		   comment '创建时间（年-月-日）',
    year_code       string		   comment '年，例如2021',
    year_month      string		   comment '年月，例如2021-05',
    time_type        string          comment '统计时间维度：year、month、day',

    customer_type         int          comment '客户类型：1-线上、0-线下',
    clue_state            int    comment '线索状态：1-新客户、0-老客户',
    origin_channel        string        comment '来源渠道',
    area                  string        comment '所在区域',
    sc_name               string        comment '校区名称',
    sj_name               string        comment '学科名称',
    dp_name               string        comment '部门名称',
    group_type            string        comment '分组类型',
    customer_cnt          bigint        comment '意向用户个数'
)comment '意向主题宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

drop table if exists onl_edu_dm.dm_clue_appeal;
create table if not exists onl_edu_dm.dm_clue_appeal(
    dt				string		   comment '创建时间（年月日）',
    hour_code       string		   comment '小时，例如20',
    year_code       string		   comment '年，例如2021',
    year_month      string		   comment '年月，例如2021-05',
    time_type        string          comment '统计时间维度',
    clue_state               int    comment '线索状态：1-新客户、0-老客户',
    customer_type           int          comment '客户类型：1-线上、0-线下',
    valid_appeal_cnt        bigint        comment '有效线索个数',
    appeal_valid_rate       decimal(5,2)    comment '有效线索转化率'
)comment '线索主题宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

-- 建表
drop table if exists onl_edu_dm.dm_signup;
create table onl_edu_dm.dm_signup(
   date_time string COMMENT '统计日期,用来标记你哪天干活的',  --'2023-05-31'
   --时间粒度标记
   time_type string COMMENT '统计时间维度：year、month、week、date(就是天day)',
   --时间粒度字段
   year_code string COMMENT '年,如2014',
   year_month string COMMENT '年月,如201401',
   month_code string COMMENT '月份,如01',
   create_time string COMMENT '时间',
   origin_channel              string comment '来源渠道',
    itcast_school_name          string comment '校区name',
    itcast_subject_name         string comment '学科name',
    origin_type                 string comment '数据来源',
    group_type                  string comment '分组类型',
    creator_depart_name         string comment '创建人部门名称',
    signup_cnt                  bigint comment '报名人数',
    potential_cnt               bigint comment '意向人数',
    clue_cnt                    bigint comment '有效线索人数',
    potential_to_signup_percent decimal(23, 2) comment '意向转报名率',
    clue_to_signup_percent      decimal(23, 2) comment '有效线索报名转换率'
)
comment '报名分析主题dm统计表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'snappy');

-- DM层
create table hive.onl_edu_dm.absent_table as
with a as (
    -- 从dws层的考勤和迟到表中找到正常打卡和迟到的人数
select class_id,
       signin_date,
       studying_student_count,
       morning_success_sign,
       morning_late_sign,
       afternoon_success_sign,
       afternoon_late_sign,
       evening_success_sign,
       evening_late_sign
from hive.onl_edu_dws.attendance_late_table),
     b as (
         -- 从请假表中找到请假的人数
select class_id,
       class_date,
       studying_student_count,
       morning_leave_cnt,
       afternoon_leave_cnt,
       evening_leave_cnt
from hive.onl_edu_dws.leave_table),
  c as (
      -- 把出勤和迟到表和请假表通过班级和时间字段关联到一起，并清洗空值数据
select
    a.*,
    if (morning_leave_cnt is not null ,morning_leave_cnt,0) as  morning_leave_cnt,
    if(afternoon_leave_cnt is not null ,afternoon_leave_cnt,0) as afternoon_leave_cnt,
    if(evening_leave_cnt is not null ,evening_leave_cnt,0) evening_leave_cnt
from a full  join b on a.class_id=b.class_id and a.signin_date=b.class_date)
-- 旷课人数=总人数-出勤人数-迟到人数-请假人数
-- 旷课率=旷课人数/总人数
select
       class_id,
       signin_date,
       studying_student_count,
       studying_student_count - morning_success_sign-morning_late_sign- morning_leave_cnt as morning_absent_cnt,
       studying_student_count -  afternoon_success_sign - afternoon_late_sign-afternoon_leave_cnt as afternoon_absent_cnt,
        studying_student_count-afternoon_success_sign-afternoon_late_sign-evening_leave_cnt as evening_absent_cnt,
       -- 旷课率
         cast (cast (studying_student_count - morning_success_sign-morning_late_sign- morning_leave_cnt  as decimal (38,10))/cast (studying_student_count as decimal (38,10))  as decimal (38,2))as morning_absent_ratio,
         cast(cast (studying_student_count -  afternoon_success_sign - afternoon_late_sign-afternoon_leave_cnt as decimal (38,10))/cast (studying_student_count as decimal (38,10)) as decimal (38,2)) as  afternoon_absent_ratio,
         cast(cast(studying_student_count-afternoon_success_sign-afternoon_late_sign-evening_leave_cnt as decimal (38,10)) /cast(studying_student_count  as decimal (38,10)) as decimal (38,2)) as evening_absent_ratio
--         concat(cast (cast (cast (studying_student_count - morning_success_sign-morning_late_sign- morning_leave_cnt  as decimal (38,10))/cast (studying_student_count as decimal (38,10))  as decimal (38,2))*100 as varchar ),'%')as morning_absent_ratio,
--          concat (cast (cast(cast (studying_student_count -  afternoon_success_sign - afternoon_late_sign-afternoon_leave_cnt as decimal (38,10))/cast (studying_student_count as decimal (38,10)) as decimal (38,2))*100 as varchar),'%' ) as  afternoon_absent_ratio,
--         concat(cast (cast(cast(studying_student_count-afternoon_success_sign-afternoon_late_sign-evening_leave_cnt as decimal (38,10)) /cast(studying_student_count  as decimal (38,10)) as decimal (38,2))*100 as varchar ),'%') as evening_absent_ratio
from c;


create table hive.onl_edu_dm.attendance_late_table as
select
       class_id,
       signin_date,
       studying_student_count,
       morning_success_sign,
       morning_late_sign,
       afternoon_success_sign,
       afternoon_late_sign,
       evening_success_sign,
       evening_late_sign,
       morning_sign_ratio,
       afternoon_sign_ratio,
       evening_sign_artio,
       morning_late_ratio,
       afternoon_late_ratio,
       evening_late_ratio
from hive.onl_edu_dws.attendance_late_table;
create table hive.onl_edu_dm.leave_table as
select * from hive.onl_edu_dws.leave_table;
