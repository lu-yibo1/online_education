create database if not exists onl_edu_ods;
create database if not exists onl_edu_dwd;
create database if not exists onl_edu_dwb;
create database if not exists onl_edu_dws;
create database if not exists onl_edu_dm;
create database if not exists onl_edu_rpt;


create table if not exists onl_edu_ods.customer(
    id                       int ,
    customer_relationship_id int      comment '当前意向id',
    create_date_time         string   comment '创建时间',
    update_date_time         string   comment '最后更新时间',
    deleted                  int      comment '是否被删除（禁用）',
    name                     string   comment '姓名',
    idcard                   string   comment '身份证号',
    birth_year               int      comment '出生年份',
    gender                   string   comment '性别',
    phone                    string   comment '手机号',
    wechat                   string   comment '微信',
    qq                       string   comment 'qq号',
    email                    string   comment '邮箱',
    area                     string   comment '所在区域',
    leave_school_date        string   comment '离校时间',
    graduation_date          string   comment '毕业时间',
    bxg_student_id           string   comment '博学谷学员ID，可能未关联到，不存在',
    creator                  int      comment '创建人ID',
    origin_type              string   comment '数据来源',
    origin_channel           string   comment '来源渠道',
    tenant                   int    ,
    md_id                    int      comment '中台id'
) comment '学员信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

create table if not exists onl_edu_ods.customer_clue(
    id                       int,
    create_date_time         string    comment '创建时间',
    update_date_time         string    comment '最后更新时间',
    deleted                  int       comment '是否被删除（禁用）',
    customer_id              int       comment '客户id',
    customer_relationship_id int       comment '客户关系id',
    session_id               string    comment '七陌会话id',
    sid                      string    comment '访客id',
    status                   string    comment '状态（undeal待领取 deal 已领取 finish 已关闭 changePeer 已流转）',
    `user`                   string    comment '所属坐席',
    create_time              string    comment '七陌创建时间',
    platform                 string    comment '平台来源 （pc-网站咨询|wap-wap咨询|sdk-app咨询|weixin-微信咨询）',
    s_name                   string    comment '用户名称',
    seo_source               string    comment '搜索来源',
    seo_keywords             string    comment '关键字',
    ip                       string    comment 'IP地址',
    referrer                 string    comment '上级来源页面',
    from_url                 string    comment '会话来源页面',
    landing_page_url         string    comment '访客着陆页面',
    url_title                string    comment '咨询页面title',
    to_peer                  string    comment '所属技能组',
    manual_time              string    comment '人工开始时间',
    begin_time               string    comment '坐席领取时间 ',
    reply_msg_count          int       comment '客服回复消息数',
    total_msg_count          int       comment '消息总数',
    msg_count                int       comment '客户发送消息数',
    comment                  string    comment '备注',
    finish_reason            string    comment '结束类型',
    finish_user              string    comment '结束坐席',
    end_time                 string    comment '会话结束时间',
    platform_description     string    comment '客户平台信息',
    browser_name             string    comment '浏览器名称',
    os_info                  string    comment '系统名称',
    area                     string    comment '区域',
    country                  string    comment '所在国家',
    province                 string    comment '省',
    city                     string    comment '城市',
    creator                  int       comment '创建人',
    name                     string    comment '客户姓名',
    idcard                   string    comment '身份证号',
    phone                    string    comment '手机号',
    itcast_school_id         int       comment '校区Id',
    itcast_school            string    comment '校区',
    itcast_subject_id        int       comment '学科Id',
    itcast_subject           string    comment '学科',
    wechat                   string    comment '微信',
    qq                       string    comment 'qq号',
    email                    string    comment '邮箱',
    gender                   string    comment '性别',
    level                    string    comment '客户级别',
    origin_type              string    comment '数据来源渠道',
    information_way          string    comment '资讯方式',
    working_years            string    comment '开始工作时间',
    technical_directions     string    comment '技术方向',
    customer_state           string    comment '当前客户状态',
    valid                    int       comment '该线索是否是网资有效线索',
    anticipat_signup_date    string    comment '预计报名时间',
    clue_state               string    comment '线索状态',
    scrm_department_id       int       comment 'SCRM内部部门id',
    superior_url             string    comment '诸葛获取上级页面URL',
    superior_source          string    comment '诸葛获取上级页面URL标题',
    landing_url              string    comment '诸葛获取着陆页面URL',
    landing_source           string    comment '诸葛获取着陆页面URL来源',
    info_url                 string    comment '诸葛获取留咨页URL',
    info_source              string    comment '诸葛获取留咨页URL标题',
    origin_channel           string    comment '投放渠道',
    course_id                int      ,
    course_name              string   ,
    zhuge_session_id         string   ,
    is_repeat                int       comment '是否重复线索(手机号维度) 0:正常 1：重复',
    tenant                   int       comment '租户id',
    activity_id              string    comment '活动id',
    activity_name            string    comment '活动名称',
    follow_type              int       comment '分配类型，0-自动分配，1-手动分配，2-自动转移，3-手动单个转移，4-手动批量转移，5-公海领取',
    shunt_mode_id            int       comment '匹配到的技能组id',
    shunt_employee_group_id  int       comment '所属分流员工组'
) comment '线索信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

create table if not exists onl_edu_ods.itcast_school(
    id               int ,
    create_date_time string     comment '创建时间',
    update_date_time string     comment '最后更新时间',
    deleted          int        comment '是否被删除（禁用）',
    name             string     comment '校区名称',
    code             string     ,
    tenant           int
) comment '校区信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


create table if not exists onl_edu_ods.itcast_subject(
    id               int,
    create_date_time string    comment '创建时间',
    update_date_time string    comment '最后更新时间',
    deleted          int       comment '是否被删除（禁用）',
    name             string    comment '学科名称',
    code             string   ,
    tenant           int
) comment '线索信息表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');