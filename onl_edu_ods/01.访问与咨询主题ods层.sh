-- 访问咨询主表
create table if not exists onl_edu_ods.web_chat_ems_2019_07
(
    id    int   comment '主键id',
    create_date_time    string    comment '数据创建时间',
    session_id      string     comment  '七陌sessionId',
    sid                string         comment '访客id',
    create_time   string          comment '会话创建时间',
    seo_source         string        comment '搜索来源',
    seo_keywords       string        comment '关键字',
    ip            string             comment 'IP地址',
    area             string          comment '地域',
    country         string           comment '所在国家',
    province         string          comment '省',
    city                string       comment '城市',
    origin_channel     string        comment '投放渠道',
    `user`         string ,
    manual_time           string         comment '人工开始时间',
    begin_time            string          comment '坐席领取时间 ',
    end_time                    string      comment '会话结束时间',
    last_customer_msg_time_stamp  string    comment '客户最后一条消息的时间',
    last_agent_msg_time_stamp  string comment '坐席最后一下回复的时间',
    reply_msg_count         int        comment '客服回复消息数',
    msg_count           int       comment '客户发送消息数',
    browser_name         string      comment '浏览器名称',
    os_info              string       comment '系统名称'
) comment '访问咨询表'
row format delimited
fields terminated by  '\t'
stored as orc tblproperties ('orc.compress' = 'snappy');--指定存储格式为orc 并使用snappy压缩

-- 访问咨询副表
create table  if not exists onl_edu_ods.web_chat_text_ems_2019_07(
     id     int    comment'主键',
    referrer    string   comment'上级来源页面',
    from_url    string   comment'会话来源页面',
    landing_page_url  string comment'访客着陆页面',
    url_title   string   comment'咨询页面title',
    platform_description   string   comment'客户平台信息',
    other_params   string   comment'扩展字段中数据',
    history    string    comment '历史访问记录'
)comment '访问与查询信息副表'
row format delimited
fields terminated by  '\t'
stored as orc tblproperties ('orc.compress' = 'snappy');


-- 采用sqoop语句插入
-- 主表
/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/nev?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select * from web_chat_ems_2019_07 where 1=1 and  \$CONDITIONS" \
--hcatalog-database onl_edu_ods \
--hcatalog-table web_chat_ems_2019_07 \
-m 1

-- 副表
/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/nev?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select * from web_chat_text_ems_2019_07 where 1=1 and  \$CONDITIONS" \
--hcatalog-database onl_edu_ods \
--hcatalog-table web_chat_text_ems_2019_07 \
-m 1

