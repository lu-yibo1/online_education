-- 创建ods分层，以及分层中的表，后续使用sqoop工具从mysql中导入数据
create database onl_edu_ods;
drop table if exists student_leave_apply;
create table student_leave_apply
(
    id                             int  comment '序列id',
    class_id                   int comment '班级id',
    student_id             int comment '学员id',
    audit_state            int comment '审核状态 0 待审核 1 通过 2 不通过',
    audit_person        int comment '审核人',
    audit_time             string comment '审核时间',
    audit_remark        string comment '审核备注',
    leave_type             int comment '请假类型  1 请假 2 销假',
    leave_reason         int comment '请假原因  1 事假 2 病假',
    begin_time             string comment '请假开始时间',
    begin_time_type   int comment '1：上午 2：下午',
    end_time                 string comment '请假结束时间',
    end_time_type      int comment '1：上午 2：下午',
    days                          decimal(5, 1) comment '请假/已休天数',
    cancel_state            int comment '撤销状态  0 未撤销 1 已撤销',
    cancel_time             string comment '撤销时间',
    old_leave_id            int comment '原请假id，只有leave_type =2 销假的时候才有',
    leave_remark          string comment '请假/销假说明',
    valid_state                int comment '是否有效（0：无效 1：有效）',
    create_time             string comment '创建时间'
) comment '学生请假表'
row format delimited
fields terminated by '\t'
stored as orc tblproperties ('orc.compress' ='snappy');

drop table  if exists tbh_class_time_table;
create table tbh_class_time_table
(
    id                                                int comment '主键id',
    class_id                                     int comment '班级id',
    morning_template_id          int comment '上午出勤模板',
    morning_begin_time          string comment '上午开始时间',
    morning_end_time              string comment '上午结束时间',
    afternoon_template_id      string comment '下午出勤模板id',
    afternoon_begin_time        string comment '下午开始时间',
    afternoon_end_time            string comment '下午结束时间',
    evening_template_id           int comment '晚上出勤模板id',
    evening_begin_time            string comment '晚上开始时间',
    evening_end_time                string comment '晚上结束时间',
    use_begin_date                     string comment '使用开始日期',
    use_end_date                         string comment '使用结束日期',
    create_time                             string comment '创建时间',
    create_person                        int comment '创建人',
    remark                                      string comment '备注'
) comment '班级作息时间表'
row format delimited
fields terminated by '\t'
stored as orc tblproperties ('orc.compress' ='snappy');

drop table  if exists tbh_student_signin_record;
create table tbh_student_signin_record
(
    id                               int comment '主键id',
    normal_class_flag int comment '是否正课 1 正课 2 自习',
    time_table_id         int comment '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id                    int comment '班级id',
    student_id               int comment '学员id',
    signin_time            string comment '签到时间',
    signin_date            string comment '签到日期',
    inner_flag               int comment '内外网标志  0 外网 1 内网',
    signin_type            int comment '签到类型 1 心跳打卡 2 老师补卡',
    share_state            int comment '共享屏幕状态 0 否 1是  在上午或下午段有共屏记录，则该段所有记录该字段为1，内网默认为1 外网默认为0 ',
    inner_ip                   string comment '内网ip地址'
) comment '学生打卡信息表'
row format delimited
fields terminated by '\t'
stored as orc tblproperties ('orc.compress' ='snappy');
