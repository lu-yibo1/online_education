-- 建表
drop table if exists onl_edu_dm.dm_signup;
create table onl_edu_dm.dm_signup(
   date_time string COMMENT '统计日期,用来标记你哪天干活的',  --'2023-05-31'
   --时间粒度标记
   time_type string COMMENT '统计时间维度：year、month、week、date(就是天day)',
   --时间粒度字段
   year_code string COMMENT '年,如2014',
   year_month string COMMENT '年月,如201401',
   month_code string COMMENT '月份,如01',
   create_time string COMMENT '时间',
   origin_channel              string comment '来源渠道',
    itcast_school_name          string comment '校区name',
    itcast_subject_name         string comment '学科name',
    origin_type                 string comment '数据来源',
    group_type                  string comment '分组类型',
    creator_depart_name         string comment '创建人部门名称',
    signup_cnt                  bigint comment '报名人数',
    potential_cnt               bigint comment '意向人数',
    clue_cnt                    bigint comment '有效线索人数',
    potential_to_signup_percent decimal(23, 2) comment '意向转报名率',
    clue_to_signup_percent      decimal(23, 2) comment '有效线索报名转换率'
)
comment '报名分析主题dm统计表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'snappy');