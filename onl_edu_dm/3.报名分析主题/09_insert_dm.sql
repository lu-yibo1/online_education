


insert into onl_edu_dm.dm_signup
select
       '2023-05-31' as date_time,
       case when grouping(year_code,month_code,create_time)=0
           then '天'
           when grouping(year_code,month_code)=0
           then '月'
           when grouping(year_code)=0
           then '年'
            else 'other'  end as time_type,
        year_code    ,
        case when grouping(year_month)=0
            then year_month
            else null end  year_month,
        month_code  ,
        create_time,
        origin_channel,
        itcast_school_name  ,
        itcast_subject_name  ,
        origin_type,
        case when grouping(origin_type,itcast_school_name,itcast_subject_name)=0
            then '线上线下+校区+学科'
        when grouping(origin_type,itcast_subject_name)=0
            then '线上线下+学科'
        when grouping(origin_type,itcast_school_name)=0
            then '线上线下+校区'
        when grouping(itcast_school_name)=0
            then '校区'
        when grouping(origin_type,origin_channel)=0
            then '线上线下+来源渠道'
--         when grouping(origin_type,creator,creator_depart_id,creator_depart_name)=0
        when grouping(origin_type,creator_depart_name)=0
            then '线上线下+咨询中心'
        else 'other' end  as group_type,
        creator_depart_name  ,

        sum(signup_cnt) as signup_cnt,
       sum(potential_cnt) as potential_cnt,
       sum(clue_cnt) as clue_cnt,
        null as potential_to_signup_percent ,
        null as clue_to_signup_percent
from onl_edu_dws.dws_signup_daycount
group by
grouping sets (
    -- 年
        (year_code),
        (year_code,itcast_school_name),
        (year_code,origin_type,itcast_school_name),
        (year_code,origin_type,itcast_subject_name),
        (year_code,origin_type,itcast_school_name,itcast_subject_name),
        (year_code,origin_type,origin_channel),
        (year_code,origin_type,creator_depart_name),
    -- 月
        (year_code,year_month,month_code),
        (year_code,year_month,month_code,itcast_school_name),
        (year_code,year_month,month_code,origin_type,itcast_school_name),
        (year_code,year_month,month_code,origin_type,itcast_subject_name),
        (year_code,year_month,month_code,origin_type,itcast_school_name,itcast_subject_name),
        (year_code,year_month,month_code,origin_type,origin_channel),
        (year_code,year_month,month_code,origin_type,creator_depart_name),

  -- 日
        (year_code,year_month,month_code,create_time),
        (year_code,year_month,month_code,create_time,itcast_school_name),
        (year_code,year_month,month_code,create_time,origin_type,itcast_school_name),
        (year_code,year_month,month_code,create_time,origin_type,itcast_subject_name),
        (year_code,year_month,month_code,create_time,origin_type,itcast_school_name,itcast_subject_name),
        (year_code,year_month,month_code,create_time,origin_type,origin_channel),
        (year_code,year_month,month_code,create_time,origin_type,creator_depart_name)
    )
;

-- select * from onl_edu_dm.dm_signup;
select count(*) from onl_edu_dm.dm_signup;