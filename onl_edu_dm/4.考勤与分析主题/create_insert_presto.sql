-- DM层
create table hive.onl_edu_dm.absent_table as
with a as (
    -- 从dws层的考勤和迟到表中找到正常打卡和迟到的人数
select class_id,
       signin_date,
       studying_student_count,
       morning_success_sign,
       morning_late_sign,
       afternoon_success_sign,
       afternoon_late_sign,
       evening_success_sign,
       evening_late_sign
from hive.onl_edu_dws.attendance_late_table),
     b as (
         -- 从请假表中找到请假的人数
select class_id,
       class_date,
       studying_student_count,
       morning_leave_cnt,
       afternoon_leave_cnt,
       evening_leave_cnt
from hive.onl_edu_dws.leave_table),
  c as (
      -- 把出勤和迟到表和请假表通过班级和时间字段关联到一起，并清洗空值数据
select
    a.*,
    if (morning_leave_cnt is not null ,morning_leave_cnt,0) as  morning_leave_cnt,
    if(afternoon_leave_cnt is not null ,afternoon_leave_cnt,0) as afternoon_leave_cnt,
    if(evening_leave_cnt is not null ,evening_leave_cnt,0) evening_leave_cnt
from a full  join b on a.class_id=b.class_id and a.signin_date=b.class_date)
-- 旷课人数=总人数-出勤人数-迟到人数-请假人数
-- 旷课率=旷课人数/总人数
select
       class_id,
       signin_date,
       studying_student_count,
       studying_student_count - morning_success_sign-morning_late_sign- morning_leave_cnt as morning_absent_cnt,
       studying_student_count -  afternoon_success_sign - afternoon_late_sign-afternoon_leave_cnt as afternoon_absent_cnt,
        studying_student_count-afternoon_success_sign-afternoon_late_sign-evening_leave_cnt as evening_absent_cnt,
       -- 旷课率
         cast (cast (studying_student_count - morning_success_sign-morning_late_sign- morning_leave_cnt  as decimal (38,10))/cast (studying_student_count as decimal (38,10))  as decimal (38,2))as morning_absent_ratio,
         cast(cast (studying_student_count -  afternoon_success_sign - afternoon_late_sign-afternoon_leave_cnt as decimal (38,10))/cast (studying_student_count as decimal (38,10)) as decimal (38,2)) as  afternoon_absent_ratio,
         cast(cast(studying_student_count-afternoon_success_sign-afternoon_late_sign-evening_leave_cnt as decimal (38,10)) /cast(studying_student_count  as decimal (38,10)) as decimal (38,2)) as evening_absent_ratio
--         concat(cast (cast (cast (studying_student_count - morning_success_sign-morning_late_sign- morning_leave_cnt  as decimal (38,10))/cast (studying_student_count as decimal (38,10))  as decimal (38,2))*100 as varchar ),'%')as morning_absent_ratio,
--          concat (cast (cast(cast (studying_student_count -  afternoon_success_sign - afternoon_late_sign-afternoon_leave_cnt as decimal (38,10))/cast (studying_student_count as decimal (38,10)) as decimal (38,2))*100 as varchar),'%' ) as  afternoon_absent_ratio,
--         concat(cast (cast(cast(studying_student_count-afternoon_success_sign-afternoon_late_sign-evening_leave_cnt as decimal (38,10)) /cast(studying_student_count  as decimal (38,10)) as decimal (38,2))*100 as varchar ),'%') as evening_absent_ratio
from c;


create table hive.onl_edu_dm.attendance_late_table as
select
       class_id,
       signin_date,
       studying_student_count,
       morning_success_sign,
       morning_late_sign,
       afternoon_success_sign,
       afternoon_late_sign,
       evening_success_sign,
       evening_late_sign,
       morning_sign_ratio,
       afternoon_sign_ratio,
       evening_sign_artio,
       morning_late_ratio,
       afternoon_late_ratio,
       evening_late_ratio
from hive.onl_edu_dws.attendance_late_table;
create table hive.onl_edu_dm.leave_table as
select * from hive.onl_edu_dws.leave_table;
