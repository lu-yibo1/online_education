--create database if not exists onl_edu_dm;
--创建整个主题宽表
drop table if exists onl_edu_dm.dm_attendance;
create table onl_edu_dm.dm_attendance(
    day               string         comment '天',
    class_id          int            comment '班级',
    period            string         comment '时期，时间段：morning、afternoon、evening',
    normal_signin_cnt bigint         comment '正常出勤人数',
    attendance_ratio  decimal(7,4)   comment '出勤率',
    late_signin_cnt   bigint         comment '迟到人数',
    late_attend_ratio decimal(7,4)   comment '迟到率',
    leave_cnt         bigint         comment '请假人数',
    leave_ratio       decimal(7,4)   comment '请假率',
    play_hooky_cnt    bigint         comment '旷课人数',
    play_hooky_ratio  decimal(7,4)   comment '旷课率'
)
comment '考勤主题统计表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='snappy');