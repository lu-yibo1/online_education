-- 考勤签到统计表 left join 请假统计表 left join 在读学员人数信息表
-- 旷课人数及旷课率计算
-- 整合所有需求所要的指标
insert into onl_edu_dm.dm_attendance
with tmp as (
 select  distinct   sc.day,
                    sc.class_id,
                    sc.period,
                    dc.studying_student_count,
                    sc.normal_signin_cnt,
                    cast(sc.attendance_ratio as decimal (7,4)) as attendance_ratio,
                    sc.late_signin_cnt,
                    cast(sc.late_attend_ratio as decimal (7,4)) as late_attend_ratio,
                    coalesce(lc.leave_cnt, 0)                                                                     as leave_cnt,
                    cast(coalesce(lc.leave_ratio,0) as decimal (7,4) )                                                                as leave_ratio,
                    coalesce(dc.studying_student_count - sc.normal_signin_cnt - sc.late_signin_cnt - lc.leave_cnt,
                             0)                                                                                       as play_hooky_cnt,
                    coalesce(dc.studying_student_count - sc.normal_signin_cnt - sc.late_signin_cnt - lc.leave_cnt,
                                  0)
                        /
                         cast(dc.studying_student_count as decimal(38, 10))                          as play_hooky_ratio
    from hive.onl_edu_dws.dws_signin_cnt sc
             left join hive.onl_edu_dws.dws_leave_cnt lc
                       on sc.day = lc.day and sc.class_id = lc.class_id and sc.period = lc.period
             left join hive.onl_edu_dwd.dt_class_studying_student_count dc
                       on sc.day = dc.studying_date and sc.class_id = dc.class_id

)
  select
                    tmp.day,
                    tmp.class_id,
                    tmp.period,
                    tmp.normal_signin_cnt,
                    tmp.attendance_ratio,
                    tmp.late_signin_cnt,
                    tmp.late_attend_ratio,
                    tmp.leave_cnt,
                    tmp.leave_ratio,
                    tmp.play_hooky_cnt,
                    cast(tmp.play_hooky_ratio as decimal (7,4)) as play_hooky_ratio
from tmp
order by tmp.day, tmp.class_id,
             case
                 when tmp.period = 'morning' then 1
                 when tmp.period = 'afternoon' then 2
                 when tmp.period = 'evening' then 3
                 end asc;
