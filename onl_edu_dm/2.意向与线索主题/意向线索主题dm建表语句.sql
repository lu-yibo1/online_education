-- dm层建表
drop table if exists onl_edu_dm.dm_relationship;
create table if not exists onl_edu_dm.dm_relationship(

    dt				string		   comment '创建时间（年-月-日）',
    year_code       string		   comment '年，例如2021',
    year_month      string		   comment '年月，例如2021-05',
    time_type        string          comment '统计时间维度：year、month、day',

    customer_type         int          comment '客户类型：1-线上、0-线下',
    clue_state            int    comment '线索状态：1-新客户、0-老客户',
    origin_channel        string        comment '来源渠道',
    area                  string        comment '所在区域',
    sc_name               string        comment '校区名称',
    sj_name               string        comment '学科名称',
    dp_name               string        comment '部门名称',
    group_type            string        comment '分组类型',
    customer_cnt          bigint        comment '意向用户个数'
)comment '意向主题宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

drop table if exists onl_edu_dm.dm_clue_appeal;
create table if not exists onl_edu_dm.dm_clue_appeal(
    dt				string		   comment '创建时间（年月日）',
    hour_code       string		   comment '小时，例如20',
    year_code       string		   comment '年，例如2021',
    year_month      string		   comment '年月，例如2021-05',
    time_type        string          comment '统计时间维度',
    clue_state               int    comment '线索状态：1-新客户、0-老客户',
    customer_type           int          comment '客户类型：1-线上、0-线下',
    valid_appeal_cnt        bigint        comment '有效线索个数',
    appeal_valid_rate       decimal(5,2)    comment '有效线索转化率'
)comment '线索主题宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');
