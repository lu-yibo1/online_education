-- 建表
drop table if exists onl_edu_dws.dws_signup_daycount;
create table onl_edu_dws.dws_signup_daycount
(
 -- 维度
    -- 校区

    year_code string comment '年',
    year_month string comment '年',
    month_code string comment '月',
    create_time string comment '时间',
    origin_channel string comment '来源渠道',
    itcast_school_id       string comment '校区Id',
    itcast_school_name     string comment '校区name',
    -- 学科
    itcast_subject_id      string comment '学科Id',
    itcast_subject_name    string comment '学科name',
    -- 来源
    origin_type            string comment '数据来源',
    group_type            string comment '分组类型',
    -- 咨询中心
     -- 员工信息
    creator_depart_id      int comment '创建人部门id',
    creator_depart_name    string comment '创建人部门名称',
-- 指标
    signup_cnt     bigint comment '报名人数',
    potential_cnt     bigint comment '意向人数',
    clue_cnt     bigint comment '有效线索人数',
    potential_to_signup_percent   decimal(23,2) comment '意向转报名率',
    clue_to_signup_percent      decimal(23,2)  comment '有效线索报名转换率'
    )
comment '报名分析主题日统计宽表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'snappy');


-- 查询
select * from onl_edu_dws.dws_signup_daycount;

