
select count(*) from onl_edu_dwb.dwb_signup_detail;


-- select * from onl_edu_dwb.dwb_signup_detail;
-- select count(*) from onl_edu_dwb.dwb_signup_detail;

--主题目宽表
insert into onl_edu_dws.dws_signup_daycount
with  tmp  as (
 select
    substring(dt,1,4) as  year_code,
    substring(dt,1,7)as year_month,
    substring(dt,6,2) as  month_code,
    dt as create_time,
-- 维度
    case when grouping(origin_type,itcast_school_id,itcast_subject_id)=0
            then '线上线下+校区+学科'
        when grouping(origin_type,itcast_subject_id)=0
            then '线上线下+学科'
        when grouping(origin_type,itcast_school_id)=0
            then '线上线下+校区'
        when grouping(itcast_school_id)=0
            then '校区'
        when grouping(origin_type,origin_channel)=0
            then '线上线下+来源渠道'
--         when grouping(origin_type,creator,creator_depart_id,creator_depart_name)=0
        when grouping(origin_type,creator_depart_name)=0
            then '线上线下+咨询中心'
        else 'other' end  as group_type,
    -- 线上线下
    case when grouping(origin_type)=0
        then if(origin_type = 'NETSERVICE','线上','线下')
        else null
        end  as origin_type,
    -- 校区
    case when grouping(itcast_school_id) = 0
        then  itcast_school_id
		else null end as itcast_school_id ,

    case when grouping(itcast_school_name) = 0
        then  itcast_school_name
		else null end as itcast_school_name ,
--     -- 学科
    case when grouping(itcast_subject_id) = 0
        then  itcast_subject_id
		else null end as itcast_subject_id ,
    case when grouping(itcast_subject_name) = 0
        then  itcast_subject_name
		else null end as itcast_subject_name ,

--     -- 来源渠道
    case when grouping(origin_channel) = 0
        then  if( origin_channel is not null,origin_channel,'-')
		else null end as origin_channel ,

    -- 咨询中心
--     case when grouping(creator) = 0
--         then  creator
-- 		else null end as creator ,

    case when grouping(creator_depart_id) = 0
        then  creator_depart_id
		else null end as creator_depart_id ,

    case when grouping(creator_depart_name) = 0
        then  creator_depart_name
		else null end as creator_depart_name ,


-- 指标
    -- 报名人数
    case when grouping(dt) = 0
        then  sum(if(payment_state='PAID',1,0))
		else null end as signup_cnt ,

    --意向人数
    case when grouping(dt) = 0
        then  sum(1)   --所有的都是意向人数
		else null end as potential_cnt ,

     -- 有效线索人数
     case when grouping(dt) = 0
        then  sum(if(appeal_status=2,1,0))   --所有的都是意向人数
		else null end as clue_cnt ,

     -- 意向转报名率
        null as potential_to_signup_percent,

     -- 有效线索报名转换率
        null as clue_to_signup_percent,
    '2023--5-31' as dt

from onl_edu_dwb.dwb_signup_detail
group by
grouping sets(
    dt, -- 时间
--     (dt,origin_type), -- 时间
    (dt,itcast_school_id,itcast_school_name), -- 时间+校区
    (dt,origin_type,itcast_school_id,itcast_school_name), -- 时间+线上线下+校区
    (dt,origin_type,itcast_subject_id,itcast_subject_name), -- 时间+线上线下+学科
    (dt,origin_type,itcast_school_id,itcast_school_name,itcast_subject_id,itcast_subject_name), -- 时间+线上线下+校区+学科

    (dt,origin_type,origin_channel), -- 时间+线上线下+来源渠道
--     (dt,origin_type,creator,creator_depart_id,creator_depart_name) -- 时间+线上线下+咨询中心
    (dt,origin_type,creator_depart_id,creator_depart_name) -- 时间+线上线下+咨询中心
)
)
-- select group_type, count(*) from tmp group by  group_type;
select year_code,
       year_month,
       month_code,
       create_time,
       group_type,
       origin_type,
       itcast_school_id,
       itcast_school_name,
       itcast_subject_id,
       itcast_subject_name,
       origin_channel,
       creator_depart_id,
       creator_depart_name,
       signup_cnt,
       potential_cnt,
       clue_cnt,
       if(potential_cnt=0 or signup_cnt=0,0, signup_cnt/(potential_cnt*1.00) ) as potential_to_signup_percent,
       if(clue_cnt=0 or signup_cnt=0,0, signup_cnt/(clue_cnt*1.00) )  as clue_to_signup_percent,
       dt
from tmp
;

select * from onl_edu_dwb.dwb_signup_detail
--          where creator is null
--            where creator_depart_id is null
           where creator_depart_name is null

;

select count(*) from onl_edu_dws.dws_signup_daycount;