-- DWS层
-- 分别列出字段、标记上下午 晚上 请假的
create table hive.onl_edu_dws.student_leave_apply_wide as
select
            id,
            class_id,
            student_id,
            audit_state,
            leave_type,
            begin_time,
            begin_time_type,
            end_time,
            end_time_type,
            days,
            cancel_state,
            valid_state,
            class_date,
            content,
            morning_template_id,
            morning_begin_time,
            morning_end_time,
            afternoon_template_id,
            afternoon_begin_time,
            afternoon_end_time,
            evening_template_id,
            evening_begin_time,
            evening_end_time,
            use_begin_date,
            use_end_date,
            studying_student_count,
            studying_date,
            case when morning_begin_time >=substring (begin_time,12,8)
            and morning_end_time <= substring (end_time,12,8)
            then '1'  --上午请假
            else '0'
            end as morning_leave,
             case when afternoon_begin_time >=substring (begin_time,12,8)
            and afternoon_end_time <= substring (end_time,12,8)
            then '1'  -- 下午请假
            else '0'
            end as afternoon_leave,
             case when evening_begin_time >=substring (begin_time,12,8)
            and evening_end_time <= substring (end_time,12,8)
            then '1'  -- 晚上请假
            else '0'
            end as evening_leave
from hive.onl_edu_dwb.ft_student_leave_apply_wide;

-- 分别列出字段、标记正常打卡和迟到的
create table hive.onl_edu_dws.tbh_student_signin_record_wide as
select
        id,
       time_table_id,
       class_id,
       student_id,
       signin_time,
       signin_date,
       inner_flag,
       signin_type,
       studying_student_count,
       studying_date,
        class_date,
        content,
         morning_template_id,
         morning_begin_time,
         morning_end_time,
         afternoon_template_id,
         afternoon_begin_time,
         afternoon_end_time,
         evening_template_id,
         evening_begin_time,
         evening_end_time,
         use_begin_date,
         use_end_date,
        case when cast(substring (signin_time,12,8) as time ) >= (cast (morning_begin_time as time) - interval '40' minute)
        and  cast(substring (signin_time,12,8) as time ) <=  (cast (morning_begin_time as time) + interval '10' minute)
        then 1 --上午正常打卡
        when cast(substring (signin_time,12,8) as time ) >  (cast (morning_begin_time as time) + interval '10' minute)
        and  cast(substring (signin_time,12,8) as time ) <= cast (morning_end_time as time)
        then  2  --上午迟到打卡
        else 3    -- 上午没打卡
        end as morning_sign,
        case when cast(substring (signin_time,12,8) as time) >=(cast (afternoon_begin_time as time ) -interval '40' minute )
        and cast (substring (signin_time,12,8) as time) <=(cast (afternoon_begin_time as time ) + interval '10' minute )
        then  1 --   下午正常打卡
        when cast(substring (signin_time,12,8) as time ) >  (cast (afternoon_begin_time as time) + interval '10' minute)
        and  cast(substring (signin_time,12,8) as time ) <= cast (afternoon_end_time as time)
        then  2   -- 下午迟到打卡
        else 3 -- 下午没打卡
        end as afternoon_sign,
         case when cast(substring (signin_time,12,8) as time) >=( cast (evening_begin_time as time ) - interval '40' minute )
          and cast(substring (signin_time,12,8) as time) <= (cast (evening_begin_time as time ) + interval '10' minute )
          then  1 --晚上正常打卡
         when cast(substring (signin_time,12,8) as time ) >  (cast (evening_begin_time as time) + interval '10' minute)
        and   cast(substring (signin_time,12,8) as time ) <= cast (evening_end_time as time)
            then 2 -- 晚上迟到打卡
          else 3   -- 晚上没打卡
          end as evening_sign
from hive.onl_edu_dwb.ft_tbh_student_signin_record_wide;


create table hive.onl_edu_dws.attendance_late_table as
with a as (     -- 去重，对每个打卡时间段重复打卡的只取一次生效
    select id,
           time_table_id,
           class_id,
           student_id,
           signin_time,
           signin_date,
           inner_flag,
           signin_type,
           studying_student_count,
           studying_date,
           class_date,
           content,
           morning_template_id,
           morning_begin_time,
           morning_end_time,
           afternoon_template_id,
           afternoon_begin_time,
           afternoon_end_time,
           evening_template_id,
           evening_begin_time,
           evening_end_time,
           use_begin_date,
           use_end_date,
           morning_sign,
           afternoon_sign,
           evening_sign,
           -- 对同一天打卡进行去重，过滤掉无效的重复打卡，1为正常出勤，2为迟到，3为无效
           row_number() over (partition by class_id,class_date,student_id order by morning_sign) as rn_morning,
           row_number() over (partition by class_id,class_date,student_id  order by afternoon_sign) as rn_afternoon,
           row_number() over (partition by class_id,class_date,student_id  order by evening_sign) as rn_evening
    from hive.onl_edu_dws.tbh_student_signin_record_wide),
b as (
      --对班级和日期分组，汇总早中晚正常出勤打卡，以及早中晚迟到打卡的人数汇总统计
select
        class_id,
       signin_date,
       min(studying_student_count) as studying_student_count ,
       count(if(morning_sign = 1 and  rn_morning = 1, 1, null))   as morning_success_sign,
       count(if(morning_sign = 2 and  rn_morning = 1, 1, null))   as morning_late_sign,
       count(if(afternoon_sign = 1 and  rn_afternoon =1 , 1, null)) as afternoon_success_sign,
       count(if(afternoon_sign = 2 and  rn_afternoon = 1, 1, null)) as afternoon_late_sign,
       count(if(evening_sign = 1 and  rn_evening =1 , 1, null))   as evening_success_sign,
       count(if(evening_sign =2  and  rn_evening = 1,1, null))   as evening_late_sign
from a
group by class_id, signin_date)
--  上一个cte语句已经对班级和日期分过组了，并汇总了正常打卡和迟到打卡的人数，在这算出正常出勤率，和迟到率
select
       class_id,
       signin_date,
       -- 每个班有多少人
       studying_student_count ,
       -- 正常出勤人数、迟到人数
       morning_success_sign,
       morning_late_sign,
       afternoon_success_sign,
       afternoon_late_sign,
       evening_success_sign,
       evening_late_sign,
       -- 正常出勤率
              -- 正常出勤率
      cast(cast(morning_success_sign as decimal (38,10))/ cast (studying_student_count as decimal (38,10))  as decimal (38,2))as morning_sign_ratio,  -- 早上出勤率
      cast (cast(afternoon_success_sign as decimal (38,10))/ cast(studying_student_count as decimal (38,10)) as decimal (38,2))as afternoon_sign_ratio, -- 下午出勤率
      cast(cast (evening_success_sign as decimal (38,10))/ cast(studying_student_count as  decimal (38,10)) as decimal (38,2))as evening_sign_artio, --晚上出勤率
     -- 迟到率
      cast(cast(morning_late_sign as decimal (38,10)) / cast(studying_student_count as decimal (38,10)) as decimal (38,2)) as morning_late_ratio,  -- 早上迟到率
      cast(cast(afternoon_late_sign as decimal (38,10))/  cast(studying_student_count as decimal(38,10)) as decimal (38,2))as afternoon_late_ratio, -- 下午迟到率
      cast(cast(evening_late_sign as decimal (38,10))/  cast( studying_student_count as decimal(38,10)) as decimal (38,2)) as evening_late_ratio --晚上迟到率
--       concat(cast(cast(cast(morning_success_sign as decimal (38,10))/ cast (studying_student_count as decimal (38,10))  as decimal (38,2))* 100 as varchar),'%')as morning_sign_ratio,  -- 早上出勤率
--       concat(cast(cast (cast(afternoon_success_sign as decimal (38,10))/ cast(studying_student_count as decimal (38,10)) as decimal (38,2))* 100 as varchar),'%')as afternoon_sign_ratio, -- 下午出勤率
--       concat(cast(cast(cast (evening_success_sign as decimal (38,10))/ cast(studying_student_count as  decimal (38,10)) as decimal (38,2))* 100 as varchar ),'%')as evening_sign_artio, --晚上出勤率
--      -- 迟到率
--       concat(cast (cast(cast(morning_late_sign as decimal (38,10)) / cast(studying_student_count as decimal (38,10)) as decimal (38,2))* 100 as varchar ),'%') as morning_late_ratio,  -- 早上迟到率
--       concat(cast(cast(cast(afternoon_late_sign as decimal (38,10))/  cast(studying_student_count as decimal(38,10)) as decimal (38,2))* 100 as varchar),'%')as afternoon_late_ratio, -- 下午迟到率
--       concat(cast (cast(cast(evening_late_sign as decimal (38,10))/  cast( studying_student_count as decimal(38,10)) as decimal (38,2))* 100 as varchar),'%') as evening_late_ratio --晚上迟到率
from b;


create table hive.onl_edu_dws.leave_table as
-- 每天 上下午晚上请假人数数据汇总
with a as (
select
            class_id,
            class_date,
            min(studying_student_count) as studying_student_count ,
            sum(if(morning_leave='1',1,0)) as morning_leave_cnt,
            sum(if(afternoon_leave='1',1,0)) as afternoon_leave_cnt,
            sum(if(evening_leave='1',1,0)) as evening_leave_cnt
from hive.onl_edu_dws.student_leave_apply_wide
group by class_id,class_date)
-- 求出每天上下午晚上的请假率
select
            class_id,
            class_date,
            studying_student_count ,
            morning_leave_cnt,
            afternoon_leave_cnt,
            evening_leave_cnt,
            cast(cast (morning_leave_cnt as decimal (38,10))/cast(studying_student_count as decimal(38,10))  as decimal (38,2))as morning_leave_ratio,
            cast(cast(afternoon_leave_cnt as decimal (38,10))/cast (studying_student_count as decimal (38,10)) as decimal (38,2)) as afternoon_leave_ratio,
            cast(cast (evening_leave_cnt as decimal (38,10))/cast (studying_student_count as decimal (38,10)) as decimal (38,2)) as evening_leave_ratio
--             concat(cast(cast(cast (morning_leave_cnt as decimal (38,10))/cast(studying_student_count as decimal(38,10))  as decimal (38,2))*100 as varchar ),'%')as morning_leave_ratio,
--             concat(cast(cast(cast(afternoon_leave_cnt as decimal (38,10))/cast (studying_student_count as decimal (38,10)) as decimal (38,2))*100 as varchar),'%') as afternoon_leave_ratio,
--             concat(cast(cast(cast (evening_leave_cnt as decimal (38,10))/cast (studying_student_count as decimal (38,10)) as decimal (38,2))*100 as varchar ),'%')as evening_leave_ratio
from a;
