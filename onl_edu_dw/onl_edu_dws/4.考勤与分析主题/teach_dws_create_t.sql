--在hive中建立dws层考勤统计表
drop table if exists onl_edu_dws.dws_signin_cnt;
create table onl_edu_dws.dws_signin_cnt(
    day              string         comment '天',
    class_id         int            comment '班级',
    period           string         comment '时期，时间段：morning、afternoon、evening',

    normal_signin_cnt bigint        comment '正常出勤人数',
    attendance_ratio  decimal(38,5)  comment '出勤率',
    late_signin_cnt   bigint        comment '迟到人数',
    late_attend_ratio decimal(38,5)  comment '迟到率'
)
comment '考勤签到统计表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='snappy');


drop table if exists onl_edu_dws.dws_leave_cnt;
create table onl_edu_dws.dws_leave_cnt(
    day              string         comment '天',
    class_id         int            comment '班级',
    period           string         comment '时期，时间段：morning、afternoon、evening',

    leave_cnt    bigint             comment '请假人数',
    leave_ratio  decimal(38,5)       comment '请假率'
)
comment '考勤请假统计表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='snappy');

