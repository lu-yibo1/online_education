insert into hive.onl_edu_dws.dws_signin_cnt
with tmp as (
--字段提取及数值转换便于计算
    select student_id,
           class_id,
           studying_student_count,
           signin_date,
           signin_time                                               as time1,
           signin_time                                               as time2,
           cast(morning_end_time as time)                            as end_mon,
           cast(afternoon_end_time as time)                          as end_aft,
           cast(evening_end_time as time)                            as end_eve,
           cast(morning_begin_time as time) - interval '40' minute   as upper_limit_mon,
           cast(morning_begin_time as time) + interval '10' minute   as lower_limit_mon,
           cast(afternoon_begin_time as time) - interval '40' minute as upper_limit_aft,
           cast(afternoon_begin_time as time) + interval '10' minute as lower_limit_aft,
           cast(evening_begin_time as time) - interval '40' minute   as upper_limit_eve,
           cast(evening_begin_time as time) + interval '10' minute   as lower_limit_eve,
           cast(substring(signin_time, 12, 19) as time)              as s_time
    from hive.onl_edu_dwb.dwb_signin_detail
),
s1 as (     select signin_date,
            class_id,
            (case
                when s_time between upper_limit_mon and end_mon then 'morning'
                when s_time between upper_limit_aft and end_aft then 'afternoon'
                when s_time between upper_limit_eve and end_eve then 'evening'
               end)       as period,
-- 添加时间段作为列：上午 、 下午 、 晚上
--正常出勤人数
     count(distinct
                 case
                     when s_time between upper_limit_mon and lower_limit_mon then student_id
                     when s_time between upper_limit_aft and lower_limit_aft then student_id
                     when s_time between upper_limit_eve and lower_limit_eve then student_id
                     end)                                                 as normal_signin_cnt,
     count(distinct
                 case
                     when s_time between upper_limit_mon and lower_limit_mon then student_id
                     when s_time between upper_limit_aft and lower_limit_aft then student_id
                     when s_time between upper_limit_eve and lower_limit_eve then student_id
                     end) / cast(studying_student_count as decimal(38, 5))
                     as attendance_ratio,
-- 找出正常打卡学生的id
--             (select distinct student_id
--              from tmp
--              where student_id = (case when s_time between upper_limit_mon and lower_limit_mon then student_id end)
--             ) as student_mon,
--
--             (select distinct student_id
--              from tmp
--              where student_id = (case when s_time between upper_limit_aft and lower_limit_aft then student_id end)
--             ) as student_aft,
--
--             (  select distinct student_id
--               from tmp
--               where student_id = (case when s_time between upper_limit_eve and lower_limit_eve then student_id end)
--             ) as student_eve,

-- 迟到学生按时间段判断，同时去重，以及去除正常打卡学生（正常打卡学生也可能在迟到时间内打卡）
     count(distinct
             case
                 when (s_time > lower_limit_mon and s_time <= end_mon and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_mon and lower_limit_mon
                                                 then student_id end)
                 )) then student_id
                 when (s_time > lower_limit_aft and s_time <= end_aft and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_aft and lower_limit_aft
                                                 then student_id end)
                 )
                     ) then student_id
                 when (s_time > lower_limit_eve and s_time <= end_eve and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_eve and lower_limit_eve
                                                 then student_id end)
                 )
                     ) then student_id
                 end) as late_signin_cnt,
    count(distinct
             case
                 when (s_time > lower_limit_mon and s_time <= end_mon and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_mon and lower_limit_mon
                                                 then student_id end)
                 )) then student_id
                 when (s_time > lower_limit_aft and s_time <= end_aft and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_aft and lower_limit_aft
                                                 then student_id end)
                 )
                     ) then student_id
                 when (s_time > lower_limit_eve and s_time <= end_eve and student_id not in (
                     select distinct student_id
                     from tmp
                     where student_id = (case
                                             when s_time between upper_limit_eve and lower_limit_eve
                                                 then student_id end)
                 )
                     ) then student_id
                 end) / cast(studying_student_count as decimal(38, 5)) as late_attend_ratio
from tmp
group by signin_date, class_id, studying_student_count,
     case when s_time between upper_limit_mon and end_mon then 'morning'
             when s_time between upper_limit_aft and end_aft then 'afternoon'
             when s_time between upper_limit_eve and end_eve then 'evening'
             end
order by signin_date)

select  s1.signin_date,
        s1.class_id,
        s1.period,
        s1.normal_signin_cnt,
        s1.attendance_ratio,
        s1.late_signin_cnt,
        s1.late_attend_ratio
from s1
where period is not null -- 去除period为null的情况
order by signin_date,
case period
           when 'morning' then 1
           when 'afternoon' then 2
           else 3
         end asc;


--grouping sets
insert into hive.onl_edu_dws.dws_leave_cnt
with tmp as (
    select class_date,
           class_id,
           student_id,
           audit_state,
           studying_student_count,
           cancel_state,
           valid_state,
           cast(substring(begin_time, 12, 19) as time) as l_beg_time,
           cast(substring(end_time, 12, 19) as time)   as l_end_time,
           cast(morning_begin_time as time)            as begin_mon,
           cast(afternoon_begin_time as time)          as begin_aft,
           cast(evening_begin_time as time)            as begin_eve,
           cast(morning_end_time as time)              as end_mon,
           cast(afternoon_end_time as time)            as end_aft,
           cast(evening_end_time as time)              as end_eve,
           'morning'                                   as am,
           'afternoon'                                 as pm,
           'evening'                                   as eve
    from hive.onl_edu_dwb.dwb_leave_detail
)
select class_date,
       class_id,
       case
           when grouping(class_date, class_id, studying_student_count, am) = 0 then 'morning'
           when grouping(class_date, class_id, studying_student_count, pm) = 0 then 'afternoon'
           when grouping(class_date, class_id, studying_student_count, eve) = 0 then 'evening'
           end as period,

       case
           when grouping(class_date, class_id, studying_student_count, am) = 0
               then count(distinct if(begin_mon >= l_beg_time and end_mon <= l_end_time, student_id, null))
           when grouping(class_date, class_id, studying_student_count, pm) = 0
               then count(distinct if(begin_aft >= l_beg_time and end_aft <= l_end_time, student_id, null))
           when grouping(class_date, class_id, studying_student_count, eve) = 0
               then count(distinct if(begin_eve >= l_beg_time and end_eve <= l_end_time, student_id, null))
           end as leave_cnt,
--            count(case
--                 when begin_mon >= l_beg_time and end_mon  <= l_end_time then student_id
--                 when begin_aft >= l_beg_time and end_aft  <= l_end_time then student_id
--                 when begin_eve >= l_beg_time and end_eve  <= l_end_time then student_id
--                end) as leave_cnt
       case
           when grouping(class_date, class_id, studying_student_count, am) = 0
               then
                   count(distinct if(begin_mon >= l_beg_time and end_mon <= l_end_time, student_id, null))
                                 / cast(studying_student_count as decimal(38, 5))
           when grouping(class_date, class_id, studying_student_count, pm) = 0
               then
                   count(distinct if(begin_aft >= l_beg_time and end_aft <= l_end_time, student_id, null))
                                 / cast(studying_student_count as decimal(38, 5))
           when grouping(class_date, class_id, studying_student_count, eve) = 0
               then
                   count(distinct if(begin_eve >= l_beg_time and end_eve <= l_end_time, student_id, null)
                                 ) / cast(studying_student_count as decimal(38, 5))
           end as leave_ratio
from tmp
where audit_state = 1
  and cancel_state = 0
  and valid_state = 1
group by
    grouping sets ( (class_date, class_id, studying_student_count, am),
                    (class_date, class_id, studying_student_count, pm),
                    (class_date, class_id, studying_student_count, eve)
    )
order by
case when period = 'morning' then 1
when period = 'afternoon' then 2
when period = 'evening' then 3
end asc;