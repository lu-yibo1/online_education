create table if not exists onl_edu_dws.dws_relationship_daycount(
    dt                    string        comment '创建时间（年月日）',
    customer_type         int           comment '客户类型：1-线上、0-线下',
    clue_state            int           comment '线索状态：1-新客户、0-老客户',
    origin_type           string        comment '来源渠道',
    area                  string        comment '所在区域',
    sc_name               string        comment '校区名称',
    sj_name               string        comment '学科名称',
    dp_name               string        comment '部门名称',
    group_type			 string        comment '分组类型：area、sc_name、sj_name、origin_type、dp_name',
    customer_cnt          int           comment '用户个数'
)comment '意向分析主题日统计宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


create table if not exists onl_edu_dws.dws_clue_appeal_daycount(
    dt                      string        comment '创建时间（年月日）',
    clue_state              int         comment '线索状态：1-新客户、0-老客户',
    customer_type           int          comment '客户类型：1-线上、0-线下',
    valid_appeal_cnt        int        comment '每天线上线下新老学员有效线索个数',
    appeal_all_cnt          int       comment '总线索个数',
    appeal_valid_rate       decimal    comment '有效线索转化率'
)comment '线索分析主题日统计宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');
