insert into hive.onl_edu_dws.dws_relationship_daycount
select
    -- 维度
    dt,
    customer_type ,
    clue_state,
    origin_channel,
    area,
    sc_name,
    sj_name,
    dp_name,
   -- 分组类型
    case when grouping(area) = 0
        then 'area'
        when grouping(sc_name) = 0
        then 'school'
        when grouping(sj_name) = 0
        then 'subject'
        when grouping(origin_channel) = 0
        then 'origin_channel'
        when grouping(dp_name) = 0
        then 'department'
        else 'all'
        end
       as group_type,
       -- 计算
     count(customer_id) as customer_cnt
from hive.onl_edu_dwb.dwb_customer_relationship_detail
group by
grouping sets (
    (dt,customer_type,clue_state),
    (dt,customer_type,clue_state,area),
    (dt,customer_type,clue_state,sc_name),
    (dt,customer_type,clue_state,sj_name),
    (dt,customer_type,clue_state,origin_channel),
    (dt,customer_type,clue_state,dp_name)
    );
	
	
insert into hive.onl_edu_dws.dws_clue_appeal_daycount
select
    substring(dt, 1, 10) as dt,
       clue_state,
       customer_type,
       count(if(appeal_status = 2, customer_id, null)) as valid_appeal_cnt,
       count(customer_id) as appeal_all_cnt,
       cast(count(if(appeal_status = 2, customer_id, null)) as decimal(5,2))/ if(count(customer_id)>0, count(customer_id), 1) as appeal_valid_rate
from hive.onl_edu_dwb.dwb_clue_appeal_detail
group by substring(dt, 1, 10), clue_state, customer_type;
select * from onl_edu_dws.dws_relationship_daycount where dt = '2015-04-27' and group_type = 'all';
	