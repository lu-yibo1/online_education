-- dwd层导入数据
insert into hive.onl_edu_dwd.dt_customer
select
    id,
    customer_relationship_id,
    create_date_time,
    update_date_time,
    name,
    area,
    creator,
    origin_type,
    origin_channel
from hive.onl_edu_ods.customer;

insert into hive.onl_edu_dwd.ft_customer_clue
select
    id,
    create_date_time,
    update_date_time,
    clue_state,
    customer_id,
    customer_relationship_id
from hive.onl_edu_ods.customer_clue;

insert into hive.onl_edu_dwd.dt_employee
select
    id,
    department_id,
    department_name,
    creator,
    create_date_time,
    update_date_time,
    tdepart_id
from hive.onl_edu_ods.employee;

insert into hive.onl_edu_dwd.dt_itcast_school
select
    id,
    create_date_time,
    update_date_time,
    name
from hive.onl_edu_ods.itcast_school;


insert into hive.onl_edu_dwd.dt_itcast_subject
select
    id,
    create_date_time,
    update_date_time,
    name
from hive.onl_edu_ods.itcast_subject;

insert into hive.onl_edu_dwd.dt_scrm_department
select
    id,
    name,
    create_date_time,
    update_date_time,
    tdepart_code
from hive.onl_edu_ods.scrm_department;

insert into hive.onl_edu_dwd.dt_customer_appeal
select
    id,
    customer_relationship_first_id,
    appeal_status,
    create_date_time,
    update_date_time
from hive.onl_edu_ods.customer_appeal;

insert into hive.onl_edu_dwd.ft_customer_relationship
select
    id,
    create_date_time,
    update_date_time,
    customer_id,
    first_id,
    business_scrm_department_id,
    origin_type,
    itcast_school_id,
    itcast_subject_id,
    creator,
    origin_channel
from hive.onl_edu_ods.customer_relationship;