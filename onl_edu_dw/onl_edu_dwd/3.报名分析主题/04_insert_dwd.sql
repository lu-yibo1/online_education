--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;



--首次全量导入
INSERT overwrite TABLE onl_edu_dwd.ft_customer_relationship PARTITION (dt)
select
        id,
       create_date_time,
       update_date_time,
       deleted,
       customer_id,
       first_id,
       belonger,
       belonger_name,
       initial_belonger,
       distribution_handler,
       business_scrm_department_id,
       last_visit_time,
       next_visit_time,
       origin_type,
       itcast_school_id,
       itcast_subject_id,
       intention_study_type,
       anticipat_signup_date,
       level,
       creator,
       current_creator,
       creator_name,
       origin_channel,
       comment,
       first_customer_clue_id,
       last_customer_clue_id,
       process_state,
       process_time,
       payment_state,
       payment_time,
       signup_state,
       signup_time,
       notice_state,
       notice_time,
       lock_state,
       lock_time,
       itcast_clazz_id,
       itcast_clazz_time,
       payment_url,
       payment_url_time,
       ems_student_id,
       delete_reason,
       deleter,
       deleter_name,
       delete_time,
       course_id,
       course_name,
       delete_comment,
       close_state,
       close_time,
       appeal_id,
       tenant,
       total_fee,
       belonged,
       belonged_time,
       belonger_time,
       transfer,
       transfer_time,
       follow_type,
       transfer_bxg_oa_account,
       transfer_bxg_belonger_name,
       dt
from onl_edu_ods.customer_relationship;


-- customer_appeal
INSERT overwrite TABLE onl_edu_dwd.dt_customer_appeal PARTITION (dt)
select id,
       customer_relationship_first_id,
       employee_id,
       employee_name,
       employee_department_id,
       employee_tdepart_id,
       appeal_status,
       audit_id,
       audit_name,
       audit_department_id,
       audit_department_name,
       audit_date_time,
       create_date_time,
       update_date_time,
       deleted,
       tenant,
       dt
from onl_edu_ods.customer_appeal;

-- employee
INSERT overwrite TABLE onl_edu_dwd.dt_employee PARTITION (dt)
select id,
       email,
       real_name,
       phone,
       department_id,
       department_name,
       remote_login,
       job_number,
       cross_school,
       last_login_date,
       creator,
       create_date_time,
       update_date_time,
       deleted,
       scrm_department_id,
       leave_office,
       leave_office_time,
       reinstated_time,
       superior_leaders_id,
       tdepart_id,
       tenant,
       ems_user_name,
       dt
from onl_edu_ods.employee;

-- itcast_clazz
INSERT overwrite TABLE onl_edu_dwd.dt_itcast_clazz PARTITION (dt)
select id,
       create_date_time,
       update_date_time,
       deleted,
       itcast_school_id,
       itcast_school_name,
       itcast_subject_id,
       itcast_subject_name,
       itcast_brand,
       clazz_type_state,
       clazz_type_name,
       teaching_mode,
       start_time,
       end_time,
       comment,
       detail,
       uncertain,
       tenant,
       dt
from onl_edu_ods.itcast_clazz;


-- scrm_department
INSERT overwrite TABLE onl_edu_dwd.dt_scrm_department PARTITION (dt)
select id,
       name,
       parent_id,
       create_date_time,
       update_date_time,
       deleted,
       id_path,
       tdepart_code,
       creator,
       depart_level,
       depart_sign,
       depart_line,
       depart_sort,
       disable_flag,
       tenant,
       dt
from onl_edu_ods.scrm_department;

select * from onl_edu_dwd.dt_scrm_department;