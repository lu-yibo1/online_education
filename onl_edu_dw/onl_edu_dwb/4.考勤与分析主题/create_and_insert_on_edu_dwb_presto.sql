-- 请假宽表
create table  hive.onl_edu_dwb.ft_student_leave_apply_wide as
select
            fa.id,
            fa.class_id,
            fa.student_id,
            fa.audit_state, -- 审核状态
            fa.leave_type,
            fa.begin_time,
            fa.begin_time_type,
            fa.end_time,
            fa.end_time_type,
            fa.days,
            fa.cancel_state,
            fa.valid_state,
        --  fa.create_time,  --扔了
         --   dd.class_id,
            dd.class_date,
            dd.content,
            dt.morning_template_id,  --这个没有
            dt.morning_begin_time,
            dt.morning_end_time,
            dt.afternoon_template_id,
            dt.afternoon_begin_time,
            dt.afternoon_end_time,
            dt.evening_template_id,
            dt.evening_begin_time,
            dt.evening_end_time,
            dt.use_begin_date,
            dt.use_end_date,
            dc.studying_student_count,
            dc.studying_date
from hive.onl_edu_dwd.ft_student_leave_apply fa
join hive.onl_edu_dwd.dt_course_table_upload_detail dd on fa.class_id=dd.class_id
and dd.class_date >=substring (fa.begin_time,1,10) and dd.class_date <= substring (fa.end_time,1,10)
join hive.onl_edu_dwd.dt_tbh_class_time_table dt on fa.class_id=dt.class_id
and dd.class_date>= dt.use_begin_date and dd.class_date <= dt.use_end_date
join hive.onl_edu_dwd.dt_class_studying_student_count dc on dd.class_id=dc.class_id
and dd.class_date=dc.studying_date;

-- 签到宽表
create table hive.onl_edu_dwb.ft_tbh_student_signin_record_wide as
select
       fr.id,
       fr.time_table_id,
       fr.class_id,
       fr.student_id,
       fr.signin_time,
       fr.signin_date,
       fr.inner_flag,
       fr.signin_type,
       --dc.class_id,
       dc.studying_student_count,
       dc.studying_date,
        -- dd.base_id, 课程id
        dd.class_date,
        dd.content,
         dt.morning_template_id,
         dt.morning_begin_time,
         dt.morning_end_time,
         dt.afternoon_template_id,
         dt.afternoon_begin_time,
         dt.afternoon_end_time,
         dt.evening_template_id,
         dt.evening_begin_time,
         dt.evening_end_time,
         dt.use_begin_date,
         dt.use_end_date
from onl_edu_dwd.ft_tbh_student_signin_record fr
    join onl_edu_dwd.dt_course_table_upload_detail dd
on fr.class_id = dd.class_id and fr.signin_date = dd.class_date
 join onl_edu_dwd.dt_class_studying_student_count dc
on fr.class_id = dc.class_id and fr.signin_date = dc.studying_date
 join onl_edu_dwd.dt_tbh_class_time_table dt
on fr.class_id = dt.class_id and dd.class_date >=dt.use_begin_date
and dd.class_date<=dt.use_end_date;