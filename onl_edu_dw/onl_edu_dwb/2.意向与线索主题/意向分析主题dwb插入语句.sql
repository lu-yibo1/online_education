insert into hive.onl_edu_dwb.dwb_customer_relationship_detail
select distinct
    substring(a.create_date_time,1,10) as dt,
    a.customer_id,
    if(a.origin_type = 'NETSERVICE', 1, 0) as customer_type,
    a.origin_channel,
    b.area,
    if(c.clue_state = 'VALID_NEW_CLUES', 1, 0) as clue_state,
    d.name as sc_name,
    e.name as sj_name,
    f.tdepart_id,
    g.name as dp_name
from hive.onl_edu_dwd.ft_customer_relationship a
left join hive.onl_edu_dwd.dt_customer b on a.customer_id = b.id
left join hive.onl_edu_dwd.dt_customer_clue c on a.customer_id = c.customer_relationship_id
left join hive.onl_edu_dwd.dt_itcast_school d on a.itcast_school_id = d.id
left join hive.onl_edu_dwd.dt_itcast_subject e on a.itcast_subject_id = e.id
left join hive.onl_edu_dwd.dt_employee f on a.creator = f.id
left join hive.onl_edu_dwd.dt_scrm_department g on f.tdepart_id = g.id;

insert into hive.onl_edu_dwb.dwb_clue_appeal_detail
select distinct
    a.create_date_time as dt,
    a.customer_relationship_id,
    if(a.clue_state = 'VALID_NEW_CLUES', 1, 0) as clue_state,
    if(c.origin_type = 'NETSERVICE', 1, 0) as customer_type,
    b.appeal_status
from hive.onl_edu_dwd.ft_customer_clue a
left join hive.onl_edu_dwd.dt_customer_appeal b on a.customer_relationship_id = b.customer_relationship_first_id
left join hive.onl_edu_dwd.ft_customer_relationship c on a.customer_relationship_id = c.customer_id;