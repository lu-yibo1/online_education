create table if not exists onl_edu_dwb.dwb_customer_relationship_detail(
    -- 原来的字段
    dt                              string        comment '创建时间（年月日）',
    customer_id                     int           comment '客户id',
    customer_type                   int          comment '客户类型：1-线上、0-线下',
    origin_type                     string        comment '来源渠道',
    -- 从学员信息表关联得到的字段
    area                     string   comment '所在区域',
    -- 从线索表关联得到的字段
    clue_state               int    comment '线索状态：1-新客户、0-老客户',
    --从校区信息表关联得到字段
    sc_name             string     comment '校区名称',
    --从学科信息表关联得到字段
    sj_name             string    comment '学科名称',
    -- 从员工信息表关联得到字段
    tdepart_id          int         comment '直属部门',
    --从部门信息表关联得到字段
    dp_name             string     comment '部门名称'
) comment '意向信息宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


create table onl_edu_dwb.dwb_clue_appeal_detail(
    -- 从意向表中关联得到dt
    dt                            string    comment '创建时间',
    --原来表的字段
    customer_id                   int       comment '客户id',
    clue_state                    int       comment '线索状态：1-新客户、0-老客户',
    customer_type                 int       comment '客户类型：1-线上、0-线下',
    appeal_status                 int       comment '申诉状态，0:待稽核 1:无效 2：有效'

) comment '线索信息宽表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

