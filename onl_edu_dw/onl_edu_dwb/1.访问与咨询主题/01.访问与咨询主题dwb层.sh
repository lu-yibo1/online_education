-- dwb层 数据筛选与创建访问与咨询宽表
create table if not exists onl_edu_dwb.dwb_web_chat_ems_2019_07(
    id  int  comment'主键',
  session_id  string comment '七陌sessionId',
  sid string comment '访客id',
  create_time  string comment '会话创建时间',
  seo_source string comment '搜索来源',
  ip string  comment 'IP地址',
  area string comment '地域',
  msg_count int comment '客户发送消息数',
  origin_channel string  comment '来源渠道',
  referrer string  comment '上级来源页面',
  from_url string  comment '会话来源页面',
  landing_page_url string comment '访客着陆页面',
  url_title string  comment '咨询页面title',
  platform_description string comment '客户平台信息',
  other_params string  comment '扩展字段中数据',
  history string comment '历史访问记录'
)comment '访问与咨询宽表'
row format delimited
fields terminated by  '\t'
stored as orc tblproperties ('orc.compress' = 'snappy');

-- 采用presto  insert into语句 插入数据

insert into onl_edu_dwb.dwb_web_chat_ems_2019_07
select
      a.id,
      a.session_id,
      a.sid,
      a.create_time,
      a.seo_source,
      a.ip,
      a.area,
      a. msg_count,
      a.origin_channel,
       b.referrer,
       b.from_url,
       b.landing_page_url,
       b.url_title,
       b.platform_description,
       b.other_params,
       b.history
from onl_edu_ods.web_chat_ems_2019_07 a
left join web_chat_text_ems_2019_07 b on a.id=b.id;

