

-- 创建表
drop table if exists onl_edu_dwb.dwb_signup_detail;
create table onl_edu_dwb.dwb_signup_detail (
-- 意向信息表
    id int comment 'id',
    payment_state string comment '支付状态', -- paid 判断报名人数
    create_date_time string comment '创建时间',
    origin_type string comment '数据来源',
    itcast_clazz_id int comment '所属ems班级id',
    creator int comment '创建人',         -- 员工id 关联员工信息表
    origin_channel string comment '来源渠道',
    first_id int comment '第一条客户关系id',
-- 线索申诉信息表
    customer_relationship_first_id int comment '第一条客户关系id',
    appeal_status int comment '申诉状态，0:待稽核 1:无效 2：有效',  -- 判断有效线索人数 --只要在relation就是意向
-- 班级信息表
    itcast_school_id string comment 'ems校区ID',
    itcast_school_name string comment 'ems校区名称',
    itcast_subject_id string comment 'ems学科ID',
    itcast_subject_name string comment 'ems学科名称',
-- 员工信息表
    creator_depart_id int comment '创建人部门id',
-- 部门信息表
    creator_depart_name string comment '创建人部门名称'
)
comment '报名明细表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');

