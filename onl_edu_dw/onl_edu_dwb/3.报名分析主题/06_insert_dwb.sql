
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;


-- 优化测试  GC overhead limit exceeded
--   set mapreduce.map.memory.mb=6144;
--   set mapreduce.map.java.opts=-Xmx4608M;（根据需要更改具体值，但这对参数的比例是1:0.75）
--   set mapreduce.reduce.memory.mb = 8192;
--   set mapreduce.reduce.java.opts = -Xmx6144M;（根据需要更改具体值，但这对参数的比例是1:0.75）

set mapred.child.java.opts=-Xmx3000m;
set mapreduce.map.java.opts=-Xmx2096m; -- （根据需要更改具体值，但这对参数的比例是1:0.75）
set mapreduce.reduce.java.opts=-Xmx2096m; -- （根据需要更改具体值，但这对参数的比例是1:0.75）
set mapreduce.map.memory.mb=1096;
set mapreduce.reduce.memory.mb=1096;

-- 提升vcore的数量，加快container的计算，降低oom的几率
  set mapreduce.map.cpu.vcores = 4;
  set mapreduce.reduce.cpu.vcores = 4;

  set mapreduce.input.fileinputformat.split.maxsize = 256000000;
  set mapreduce.input.fileinputformat.split.minsize = 256000000;

-- set mapreduce.map.java.opts=-Xmx4096m -XX:+UseConcMarkSweepGC;
set hive.groupby.skewindata=true;  -- Map阶段groupby阶段防倾斜
set hive.optimize.skewjoin=true;  -- join阶段防出现数据倾斜
set hive.auto.convert.join=false;  --关闭mapjoin
-- set hive.groupby.skewindata=true;  --生成的查询计划会有两个MRJob。
set mapred.reduce.tasks=4;



insert into onl_edu_dwb.dwb_signup_detail partition (dt)
select
    cr.id,
    cr.payment_state,
    cr.create_date_time,
    cr.origin_type,
    cr.itcast_clazz_id,
    cr.creator,
    cr.origin_channel,
    cr.first_id,
    ca.customer_relationship_first_id,
    ca.appeal_status,
    ic.itcast_school_id,
    ic.itcast_school_name,
    ic.itcast_subject_id,
    ic.itcast_subject_name,
    em.tdepart_id as creator_depart_id,
    sd.name as creator_depart_name,
    '2023-05-31'as dt
from onl_edu_dwd.ft_customer_relationship cr
left join onl_edu_dwd.dt_customer_appeal ca on cr.first_id = ca.id
left join onl_edu_dwd.dt_itcast_clazz ic on cr.itcast_clazz_id = ic.id
left join onl_edu_dwd.dt_employee em on cr.creator = em.id
left join onl_edu_dwd.dt_scrm_department sd on em.tdepart_id = sd.id;
-- where cr.payment_state = 'PAID';
