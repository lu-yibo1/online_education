create database if not exists onl_edu_rpt;


-- 每天/每月/每年各个校区的报名人数
create table  mysql.onl_edu.school_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
itcast_school_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='校区';

-- 每天/每月/每年线上线下各个校区的报名人数
create table  mysql.onl_edu.origin_school_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
itcast_school_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+校区';

-- 每天/每月/每年线上线下各个学科的报名人数
create table  mysql.onl_edu.origin_subject_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
itcast_subject_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+学科';

-- 每天/每月/每年线上线下各个校区各个学科的报名人数
create table  mysql.onl_edu.origin_school_subject_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
itcast_school_name,
itcast_subject_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+校区+学科';

-- 每天/每月/每年线上线下各个来源渠道的报名人数
create table  mysql.onl_edu.origin_channel_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
origin_channel,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+来源渠道';

-- 每天/每月/每年线上线下各个咨询中心的报名人数
create table  mysql.onl_edu.origin_depart_cnt as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
creator_depart_name,
signup_cnt
from onl_edu_dm.dm_signup
where group_type='线上线下+咨询中心';


-- 每天/每月/每年线上线下的意向转报名率 = 报名人数 / 意向人数
create table  mysql.onl_edu.origin_potential_percent as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
if(potential_cnt=0 or signup_cnt=0,0, signup_cnt/(potential_cnt*1.00) ) as potential_to_signup_percent
from onl_edu_dm.dm_signup;

-- 每天/每月/每年线上的有效线索报名转换率 = 报名人数 / 有效线索人数
create table  mysql.onl_edu.origin_clue_percent as
select
date_time,
time_type,
create_time,
year_code,
year_month,
month_code,
group_type,
if(clue_cnt=0 or signup_cnt=0,0, signup_cnt/(clue_cnt*1.00) )  as clue_to_signup_percent
from onl_edu_dm.dm_signup;


