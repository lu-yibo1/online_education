-- rpt插入语句
insert into hive.onl_edu_rpt.rpt_relationship_all_cnt
select
    year_code,
    year_month,
    dt,
    time_type,
    case customer_type
        when 1 then '线上'
        when 0 then '线下'
        end
        as source_type,
    case clue_state
        when 1 then '新用户'
        when 0 then '老用户'
        end
        as customer_state,
    customer_cnt
from hive.onl_edu_dm.dm_relationship;

insert into hive.onl_edu_rpt.rpt_relationship_area_cnt
select distinct
    year_code,
    year_month,
    dt,
    time_type,
    case customer_type
        when 1 then '线上'
        when 0 then '线下'
        end
        as source_type,
    case clue_state
        when 1 then '新用户'
        when 0 then '老用户'
        end
        as customer_state,
       area,
    customer_cnt
from hive.onl_edu_dm.dm_relationship
where group_type = 'area';

insert into hive.onl_edu_rpt.rpt_relationship_subject_cnt
select
    year_code,
    year_month,
    dt,
    time_type,
    case customer_type
        when 1 then '线上'
        when 0 then '线下'
        end
        as source_type,
    case clue_state
        when 1 then '新用户'
        when 0 then '老用户'
        end
        as customer_state,
       sj_name,
    customer_cnt
from hive.onl_edu_dm.dm_relationship
where group_type = 'subject'
order by customer_cnt desc;

insert into hive.onl_edu_rpt.rpt_relationship_school_cnt
select
    year_code,
    year_month,
    dt,
    time_type,
    case customer_type
        when 1 then '线上'
        when 0 then '线下'
        end
        as source_type,
    case clue_state
        when 1 then '新用户'
        when 0 then '老用户'
        end
        as customer_state,
       sc_name,
    customer_cnt
from hive.onl_edu_dm.dm_relationship
where group_type = 'school'
order by customer_cnt desc;


insert into hive.onl_edu_rpt.rpt_relationship_origin_rate

select
    year_code,
    year_month,
    dt,
    time_type,
    case customer_type
        when 1 then '线上'
        when 0 then '线下'
        end
        as source_type,
    case clue_state
        when 1 then '新用户'
        when 0 then '老用户'
        end
        as customer_state,
       origin_channel,
    customer_cnt
from hive.onl_edu_dm.dm_relationship
where group_type = 'origin_channel';


insert into hive.onl_edu_rpt.rpt_relationship_dp_rate

select
    year_code,
    year_month,
    dt,
    time_type,
    case customer_type
        when 1 then '线上'
        when 0 then '线下'
        end
        as source_type,
    case clue_state
        when 1 then '新用户'
        when 0 then '老用户'
        end
        as customer_state,
       dp_name,
    customer_cnt
from hive.onl_edu_dm.dm_relationship
where group_type = 'department';