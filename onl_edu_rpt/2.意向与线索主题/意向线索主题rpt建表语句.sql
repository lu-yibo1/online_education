--rpt层建表语句
-- 需求1：总意向量（线型图）
create table if not exists onl_edu_rpt.rpt_relationship_all_cnt(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    customer_cnt          bigint        comment '意向用户个数'
)comment '总意向量表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

-- 需求2：意向学员位置热力图（地图热力图）
create table if not exists onl_edu_rpt.rpt_relationship_area_cnt(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    area                  string        comment '所在区域',
    customer_cnt          bigint        comment '意向用户个数'
)comment '各地区意向量表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

-- 需求3：意向学科排名（柱状图）
create table if not exists onl_edu_rpt.rpt_relationship_subject_cnt(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    sj_name               string        comment '学科名称',
    customer_cnt          bigint        comment '意向用户个数'
)comment '各学科意向量排名表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

-- 需求4：意向校区排名（柱状图）
create table if not exists onl_edu_rpt.rpt_relationship_school_cnt(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    sc_name               string        comment '校区名称',
    customer_cnt          bigint        comment '意向用户个数'
)comment '各校区意向量排名表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


-- 需求5：来源渠道占比（饼状图）
create table if not exists onl_edu_rpt.rpt_relationship_origin_rate(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    origin_channel               string        comment '来源渠道',
    customer_cnt          bigint        comment '意向用户占比'
)comment '各来源渠道意向量占比表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');

-- 需求6：意向贡献中心占比（饼状图）
create table if not exists onl_edu_rpt.rpt_relationship_dp_rate(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    dt              string         comment '天',
    time_type       string         comment '时间类型',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    dp_name               string        comment '咨询中心',
    customer_cnt          bigint        comment '意向用户占比'
)comment '各中心意向量占比表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


-- 需求7：有效线索转化率
create table if not exists onl_edu_rpt.rpt_clue_valid_msgrate(
    year_code       string		   comment '年',
    year_month      string		   comment '月',
    source_type         string          comment '客户类型：线上线下',
    customer_state      string    comment '线索状态：新老客户',
    valid_appeal_cnt        bigint        comment '有效线索个数',
    msg_valid_rate      decimal(5,2)   comment '访客咨询率'
)comment '访客线索有效转化率表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');


-- 需求8：有效线索转化率
create table if not exists onl_edu_rpt.rpt_clue_valid_hrate(
    dt				string		   comment '创建时间（年月日）',
    hour_code       string		   comment '小时，例如20',
    clue_state               int    comment '线索状态：1-新客户、0-老客户',
    customer_type           int          comment '客户类型：1-线上、0-线下',
    appeal_valid_rate       decimal(5,2)    comment '有效线索转化率'
)comment '各时间段有效线索转化率表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compass' = 'SNAPPY');