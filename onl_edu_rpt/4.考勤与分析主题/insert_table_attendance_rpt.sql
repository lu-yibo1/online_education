-- rpt插入语句
--需求1
insert into onl_edu_rpt.rpt_normal_signin_cnt
select day,
       class_id,
       period,
       normal_signin_cnt,
       attendance_ratio
from hive.onl_edu_dm.dm_attendance
order by day, class_id,
case period when 'morning' then 1
            when 'afternoon' then 2
            when 'evening' then 3
            end asc ;
--需求2
insert into onl_edu_rpt.rpt_late_signin_cnt
select day,
       class_id,
       period,
       late_signin_cnt,
       late_attend_ratio
from hive.onl_edu_dm.dm_attendance
order by day, class_id,
case period when 'morning' then 1
            when 'afternoon' then 2
            when 'evening' then 3
            end asc ;

--需求3
insert into onl_edu_rpt.rpt_leave_cnt
select day,
       class_id,
       period,
       leave_cnt,
       leave_ratio
from hive.onl_edu_dm.dm_attendance
order by day, class_id,
case period when 'morning' then 1
            when 'afternoon' then 2
            when 'evening' then 3
            end asc ;

--需求4
insert into onl_edu_rpt.rpt_play_hooky_cnt
select day,
       class_id,
       period,
       play_hooky_cnt,
       play_hooky_ratio
from hive.onl_edu_dm.dm_attendance
order by day, class_id,
case period when 'morning' then 1
            when 'afternoon' then 2
            when 'evening' then 3
            end asc ;