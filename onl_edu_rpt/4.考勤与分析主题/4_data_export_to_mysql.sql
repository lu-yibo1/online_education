---CTAS
create table mysql.edu_olap.rpt_late_signin_cnt
as select * from hive.onl_edu_rpt.rpt_late_signin_cnt;

create table mysql.edu_olap.rpt_leave_cnt
as select * from hive.onl_edu_rpt.rpt_leave_cnt;

create table mysql.edu_olap.rpt_normal_signin_cnt
as select * from hive.onl_edu_rpt.rpt_normal_signin_cnt;

create table mysql.edu_olap.rpt_play_hooky_cnt
as select * from hive.onl_edu_rpt.rpt_play_hooky_cnt;