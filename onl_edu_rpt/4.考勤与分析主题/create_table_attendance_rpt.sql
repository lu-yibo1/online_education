--rpt层建表语句
--需求1：每天每个班级各时间段的正常出勤人数、出勤率
--create database if not exists onl_edu_rpt;
drop table if exists onl_edu_rpt.rpt_normal_signin_cnt;
create table onl_edu_rpt.rpt_normal_signin_cnt(
    day               string comment '天',
    class_id          int comment '班级',
    period            string comment '时期，时间段：morning、afternoon、evening',
    normal_signin_cnt bigint comment '正常出勤人数',
    attendance_ratio  decimal(7, 4) comment '出勤率'

) comment '正常出勤统计表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'snappy');

--需求2：每天各个班级各时间段的迟到人数、迟到率
drop table if exists onl_edu_rpt.rpt_late_signin_cnt;
create table onl_edu_rpt.rpt_late_signin_cnt(
    day               string comment '天',
    class_id          int comment '班级',
    period            string comment '时期，时间段：morning、afternoon、evening',
    late_signin_cnt   bigint comment '迟到人数',
    late_attend_ratio decimal(7, 4) comment '迟到率'
) comment '迟到打卡统计表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'snappy');

--需求3：每天各个班级各时间段的请假人数、请假率
drop table if exists onl_edu_rpt.rpt_leave_cnt;
create table onl_edu_rpt.rpt_leave_cnt(
    day               string comment '天',
    class_id          int comment '班级',
    period            string comment '时期，时间段：morning、afternoon、evening',
    leave_cnt   bigint comment '请假人数',
    leave_ratio decimal(7, 4) comment '请假率'
) comment '请假人数统计表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'snappy');

--需求4：每天各个班级各时间段的旷课人数、旷课率
drop table if exists onl_edu_rpt.rpt_play_hooky_cnt;
create table onl_edu_rpt.rpt_play_hooky_cnt(
    day               string comment '天',
    class_id          int comment '班级',
    period            string comment '时期，时间段：morning、afternoon、evening',
    play_hooky_cnt    bigint comment '旷课人数',
    play_hooky_ratio  decimal(7, 4) comment '旷课率'
) comment '旷课人数统计表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'snappy');