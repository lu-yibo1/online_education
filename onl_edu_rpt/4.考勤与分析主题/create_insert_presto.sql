-- RPT层  /每天/每班/    早中晚出勤人数、出勤率
create table hive.onl_edu_rpt.attendance_table as
select
       class_id,
       signin_date,
       morning_success_sign,
       afternoon_success_sign,
       evening_success_sign,
       morning_sign_ratio,
       afternoon_sign_ratio,
       evening_sign_artio
from onl_edu_dm.attendance_late_table;


-- /每天/每班   早中晚迟到人数、迟到率
create table  hive.onl_edu_rpt.late_table as
select class_id,
       signin_date,
       morning_late_sign,
       afternoon_late_sign,
       evening_late_sign,
       morning_late_ratio,
       afternoon_late_ratio,
       evening_late_ratio
from onl_edu_dm.attendance_late_table;

-- /每天/每班  早中晚请假人数、请假率
create table hive.onl_edu_rpt.leave_table as
select
       class_id,
       class_date,
       morning_leave_cnt,
       afternoon_leave_cnt,
       evening_leave_cnt,
       morning_leave_ratio,
       afternoon_leave_ratio,
       evening_leave_ratio
from hive.onl_edu_dm.leave_table;

-- /每天/每班  旷课人数、旷课率
create table hive.onl_edu_rpt.absent_table as
select * from hive.onl_edu_dm.absent_table;